package info.appteve.lavradio.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.tmall.ultraviewpager.UltraViewPager;
import com.tmall.ultraviewpager.transformer.UltraDepthScaleTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import info.appteve.lavradio.R;
import info.appteve.lavradio.activities.HomeActivity;
import info.appteve.lavradio.activities.YouTubePlayerActivity;
import info.appteve.lavradio.adapters.radio.FeaturedImagesPagerAdapter;
import info.appteve.lavradio.adapters.radio.NewsAdapter;
import info.appteve.lavradio.adapters.radio.PodCastAdapter;
import info.appteve.lavradio.adapters.radio.RadioStreamAdapter;
import info.appteve.lavradio.api.Api;
import info.appteve.lavradio.common.Constants;
import info.appteve.lavradio.endpoints.EndpointKeys;
import info.appteve.lavradio.endpoints.EndpointUrl;
import info.appteve.lavradio.fragments.base.BaseFragment;
import info.appteve.lavradio.pojo.eventbus.EventBusFeaturedImageClick;
import info.appteve.lavradio.pojo.eventbus.EventBusMediaPlayer;
import info.appteve.lavradio.pojo.eventbus.EventBusNewsImageClick;
import info.appteve.lavradio.pojo.eventbus.EventBusOfflineStopMedia;
import info.appteve.lavradio.pojo.eventbus.EventBusPlayPodCast;
import info.appteve.lavradio.pojo.eventbus.EventBusPlayerStatus;
import info.appteve.lavradio.pojo.eventbus.EventBusPodCastClick;
import info.appteve.lavradio.pojo.eventbus.EventBusRadioName;
import info.appteve.lavradio.pojo.eventbus.EventBusRadioViewClick;
import info.appteve.lavradio.pojo.eventbus.EventBusRefreshNewsBrowser;
import info.appteve.lavradio.pojo.models.radio_models.FeaturedModel;
import info.appteve.lavradio.pojo.models.radio_models.NewsModel;
import info.appteve.lavradio.pojo.models.radio_models.PodCastModel;
import info.appteve.lavradio.pojo.models.radio_models.Preroll;
import info.appteve.lavradio.pojo.models.radio_models.RadioModel;
import info.appteve.lavradio.radio.PlaybackStatus;
import info.appteve.lavradio.radio.RadioManager;
import info.appteve.lavradio.radio.RadioService;
import info.appteve.lavradio.utils.FontUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static info.appteve.lavradio.constants.Constants.APP_PREFERENCES;
import static info.appteve.lavradio.constants.Constants.APP_PREFERENCES_NAME_IMAGE;
import static info.appteve.lavradio.constants.Constants.APP_PREFERENCES_NAME_STATION;
import static info.appteve.lavradio.constants.Constants.APP_PREFERENCES_URL_STATION;

public class RadioFragment extends BaseFragment {

    //    Android fields....
    @BindView(R.id.recyclerViewRadioStream)
    RecyclerView recyclerViewRadioStream;
    @BindView(R.id.recyclerViewNews)
    RecyclerView recyclerViewNews;
    @BindView(R.id.recyclerViewPodcast)
    RecyclerView recyclerViewPodcast;
    @BindView(R.id.textViewRadioStream)
    TextView textViewRadioStream;
    @BindView(R.id.textViewNews)
    TextView textViewNews;
    @BindView(R.id.textViewPodcast)
    TextView textViewPodCast;
    @BindView(R.id.miniPlayer)
    ConstraintLayout constraintLayout;
    @BindView(R.id.imageViewClose)
    ImageView imageViewClose;
    @BindView(R.id.imageViewPause)
    ImageView imageViewPause;
    @BindView(R.id.textViewTitle)
    TextView textViewTitle;
    @BindView(R.id.textViewArtistName)
    TextView textViewArtistName;
    @BindView(R.id.logoRadio)
    ImageView logoRadio;
    @BindView(R.id.progressBarRadio)
    ProgressBar progressBar;
    @BindView(R.id.viewPagifyFeaturedImages)
    UltraViewPager infiniteCycleViewPager;
    Unbinder unbinder;

    //    Instance fields....
    List<FeaturedModel> featuredImagesList;
    List<PodCastModel> podCastList;
    List<NewsModel> newsList;
    List<RadioModel> radioStreamList;
    RadioModel currentRadioModel;
    SharedPreferences sharedPreferences;

    // Retrofit Calls
    Call<List<FeaturedModel>> featuredModelCall;
    Call<List<NewsModel>> newsModelCall;
    Call<List<PodCastModel>> podcastCall;
    Call<List<RadioModel>> radioModelCall;

    //    Radio stream fields....
    AudioManager audioManager;
    RadioService radioService;
    RadioManager radioManager;

    //    Font fields....
    private FontUtils fontUtils;

    private static final String TAG = "RadioFragment";

    public RadioFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_radio, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        init();
    }

    public void init() {
        EventBus.getDefault().register(this);
        try {
            radioManager = RadioManager.with(getActivity());
            radioManager.bind();
            radioService = RadioManager.getService();

            AudioManager am = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
            am.setMode(AudioManager.STREAM_MUSIC);

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            sharedPreferences = getActivity().getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

            if (radioService != null) {
                if (radioService.isPlaying()) {
                    textViewTitle.setText(sharedPreferences.getString(APP_PREFERENCES_NAME_STATION, ""));
                    Glide.with(getContext())
                            .load(EndpointUrl.IMAGE_BASE_URL + "/" + sharedPreferences.getString(APP_PREFERENCES_NAME_IMAGE, ""))
                            .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                            .into(logoRadio);
                    constraintLayout.setVisibility(View.VISIBLE);
                } else {
                    constraintLayout.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {

        }

        //        Setting custom font....
        fontUtils = FontUtils.getFontUtils(getActivity());
        fontUtils.setTextViewBoldFont(textViewNews);
        fontUtils.setTextViewBoldFont(textViewPodCast);
        fontUtils.setTextViewBoldFont(textViewRadioStream);

        //        Setting layout managers....
        recyclerViewRadioStream.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewNews.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recyclerViewPodcast.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        infiniteCycleViewPager.setScrollMode(UltraViewPager.ScrollMode.HORIZONTAL);
        infiniteCycleViewPager.setInfiniteLoop(true);
        infiniteCycleViewPager.setMultiScreen(0.7f);
        infiniteCycleViewPager.setItemRatio(1.0f);
        infiniteCycleViewPager.setRatio(1.3f);
        infiniteCycleViewPager.setMaxHeight(800);
        infiniteCycleViewPager.setAutoMeasureHeight(true);
        infiniteCycleViewPager.setPageTransformer(false, new UltraDepthScaleTransformer());

        getFeatureData();
        getNewsData();
        getPodCastData();
        getRadioData();

        imageViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constraintLayout.setVisibility(View.GONE);
                if (radioManager != null) {
                    radioManager.stopPlayer();
//                    radioManager.unbind();
                }
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusFeaturedImageClick(EventBusFeaturedImageClick eventBusFeaturedImageClick) {
        String url = featuredImagesList.get(eventBusFeaturedImageClick.getPosition()).getLink();
        startYoutubePlayerActivity(url, Constants.FEATURED_IMAGE, "");
        if (radioManager != null && radioManager.isPlaying())
            radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void stopMediaFromOffline(EventBusOfflineStopMedia stopMedia) {
        if (radioManager != null && radioManager.isPlaying())
            radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusNewsImageClick(final EventBusNewsImageClick eventBusNewsImageClick) {
        ((HomeActivity) getActivity()).selectedTab(EndpointKeys.NEWS_BROWSER);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new EventBusRefreshNewsBrowser(newsList.get(eventBusNewsImageClick.getPosition()).getOutsideLink()));
            }
        }, 100);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusRadioViewClick(EventBusRadioViewClick eventBusRadioViewClick) {
        List<Preroll> prerollList = new ArrayList<>(radioStreamList.get(eventBusRadioViewClick.getPosition()).getPreroll());
        if (prerollList.size() > 0) {
            Random r = new Random();
            int randomNumber = r.nextInt(prerollList.size());
            String url = prerollList.get(randomNumber).getLink();
            String learnMore = prerollList.get(randomNumber).getLearnMore();
            startYoutubePlayerActivity(url, Constants.RADIO_IMAGE, learnMore);
            if (radioManager != null && radioManager.isPlaying())
                radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
        }
        currentRadioModel = radioStreamList.get(eventBusRadioViewClick.getPosition());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusMediaPlayerClick(EventBusMediaPlayer eventBusMediaPlayer) {
        constraintLayout.setVisibility(View.VISIBLE);
        textViewTitle.setText(currentRadioModel.getName());
        Glide.with(getContext())
                .load(EndpointUrl.IMAGE_BASE_URL + "/" + currentRadioModel.getImageFile())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                .into(logoRadio);
        sharedPreferences.edit().putString(APP_PREFERENCES_NAME_STATION, currentRadioModel.getName()).apply();
        sharedPreferences.edit().putString(APP_PREFERENCES_URL_STATION, currentRadioModel.getRadioUrl()).apply();
        sharedPreferences.edit().putString(APP_PREFERENCES_NAME_IMAGE, currentRadioModel.getImageFile()).apply();
        startRadioStreaming();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusPodCastClick(final EventBusPodCastClick eventBusPodCastClick) {
        ((HomeActivity) getActivity()).selectedTab(EndpointKeys.PODCAST_PLAYER);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new EventBusPlayPodCast(eventBusPodCastClick.getPosition(), podCastList));
                if (radioManager != null && radioManager.isPlaying())
                    radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());

            }
        }, 100);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusArtistName(EventBusRadioName eventBusRadioName) {
        if (eventBusRadioName.getName().equals("Artist not found")) {
            textViewArtistName.setText(textViewTitle.getText().toString());
        } else {
            textViewArtistName.setText(eventBusRadioName.getName());
        }
    }

    private void startYoutubePlayerActivity(String url, String playType, String learnMore) {
        Intent intent = new Intent(getActivity(), YouTubePlayerActivity.class);
        intent.putExtra(EndpointKeys.FEATURED_VIDEO_URL, url);
        intent.putExtra(EndpointKeys.PLAY_TYPE, playType);
        intent.putExtra(EndpointKeys.LEARN_MORE, learnMore);
        startActivity(intent);
    }

    private void startRadioStreaming() {
        try {
            if (radioService != null) {
                if (radioService.getStatus() == PlaybackStatus.PLAYING || radioService.getStatus() == PlaybackStatus.PAUSED) {
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
                                }
                            }, 1000);
                        }
                    });
                } else {
                    audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (radioManager != null) {
                                radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
                            }
                        }
                    }, 1000);
                }
            } else {
                radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
            }
        } catch (Exception e) {

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusPlayerStatus(EventBusPlayerStatus eventBusPlayerStatus) {
        if (getActivity() != null) {
            switch (eventBusPlayerStatus.getStatus()) {
                case "paused":
                    imageViewPause.setImageResource(R.drawable.ic_play_arrow_black);
                    break;
                case "playing":
                    imageViewPause.setImageResource(R.drawable.ic_pause_black);
                    break;
                case "stopped":
                    constraintLayout.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Subscribe
    public void onEvent(String status) {
        switch (status) {
            case PlaybackStatus.LOADING:
                break;
            case PlaybackStatus.ERROR:
                radioManager.stopPlayer();
                Toast.makeText(getActivity(), R.string.no_stream, Toast.LENGTH_SHORT).show();
                break;
            case PlaybackStatus.STOPPED:
                break;
            case PlaybackStatus.IDLE:
                break;
        }
        imageViewPause.setImageResource(status.equals(PlaybackStatus.PLAYING) ? R.drawable.ic_pause_black : R.drawable.ic_play_arrow_black);
    }

    @OnClick(R.id.imageViewPause)
    public void onPlayPauseClick() {
        radioManager.playOrPause(currentRadioModel.getRadioUrl(), currentRadioModel.getName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        radioManager.unbind();
        EventBus.getDefault().unregister(this);
        if (featuredModelCall != null && featuredModelCall.isExecuted()) {
            featuredModelCall.cancel();
        }
        if (newsModelCall != null && newsModelCall.isExecuted()) {
            newsModelCall.cancel();
        }
        if (podcastCall != null && podcastCall.isExecuted()) {
            podcastCall.cancel();
        }
        if (radioModelCall != null && radioModelCall.isExecuted()) {
            radioModelCall.cancel();
        }
    }

    //method to get data of Radio API
    public void getRadioData() {
        progressBar.setVisibility(View.VISIBLE);
        radioStreamList = new ArrayList<>();
        radioModelCall = Api.WEB_SERVICE.searchRadio(EndpointUrl.X_API_KEY);
        radioModelCall.enqueue(new Callback<List<RadioModel>>() {
            @Override
            public void onResponse(Call<List<RadioModel>> call, Response<List<RadioModel>> response) {
                progressBar.setVisibility(View.GONE);
                if (response != null && response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().size() > 0) {
                            textViewRadioStream.setVisibility(View.VISIBLE);
                            radioStreamList.addAll(response.body());
                            recyclerViewRadioStream.setAdapter(new RadioStreamAdapter(radioStreamList, getContext()));
                        } else {
                            textViewRadioStream.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<RadioModel>> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                progressBar.setVisibility(View.GONE);
                if (error != null) {
                    if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                    }
                }
            }
        });
    }

    // method to get data of featured Api.
    public void getFeatureData() {
        featuredImagesList = new ArrayList<>();
        featuredModelCall = Api.WEB_SERVICE.searchFeatured(EndpointUrl.X_API_KEY);
        featuredModelCall.enqueue(new Callback<List<FeaturedModel>>() {
            @Override
            public void onResponse(Call<List<FeaturedModel>> call, Response<List<FeaturedModel>> response) {
                if (response != null && response.isSuccessful()) {
                    featuredImagesList = response.body();
                    FeaturedImagesPagerAdapter pagerAdapter = new FeaturedImagesPagerAdapter(featuredImagesList, getContext());
                    infiniteCycleViewPager.setAdapter(pagerAdapter);
                    infiniteCycleViewPager.setOffscreenPageLimit(featuredImagesList.size() - 1);
                    for (int i = 0; i < featuredImagesList.size(); i++) {
                        if (featuredImagesList.get(i).getIsFeatured().equals("1")) {
                            infiniteCycleViewPager.setCurrentItem(i);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<FeaturedModel>> call, Throwable error) {
                if (call.isCanceled() || "Canceled".equals(error.getMessage())) {
                    return;
                }
                if (error != null) {
                    if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                    }
                }
            }
        });


    }

    //method to get data of News API
    public void getNewsData() {
        newsList = new ArrayList<>();
        newsModelCall = Api.WEB_SERVICE.searchNews(EndpointUrl.X_API_KEY);
        newsModelCall.enqueue(new Callback<List<NewsModel>>() {
            @Override
            public void onResponse(Call<List<NewsModel>> call, Response<List<NewsModel>> response) {
                if (response != null && response.isSuccessful()) {
                    newsList = response.body();
                    if (newsList.size() > 0) {
                        textViewNews.setVisibility(View.VISIBLE);
                        recyclerViewNews.setAdapter(new NewsAdapter(newsList, getContext()));
                    } else {
                        textViewNews.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<NewsModel>> call, Throwable error) {
                if (error != null) {
                    if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                    }
                }
            }
        });
    }

    //method to get data of PodCast API
    public void getPodCastData() {
        podCastList = new ArrayList<>();
        podcastCall = Api.WEB_SERVICE.searchPodCast(EndpointUrl.X_API_KEY);
        podcastCall.enqueue(new Callback<List<PodCastModel>>() {
            @Override
            public void onResponse(Call<List<PodCastModel>> call, Response<List<PodCastModel>> response) {
                if (response != null && response.isSuccessful()) {
                    podCastList = response.body();
                    if (podCastList.size() > 0) {
                        textViewPodCast.setVisibility(View.VISIBLE);
                        recyclerViewPodcast.setAdapter(new PodCastAdapter(podCastList, getContext()));
                    } else {
                        textViewPodCast.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PodCastModel>> call, Throwable error) {
                if (error != null) {
                    if (error.getMessage() != null && error.getMessage().contains("No address associated with hostname")) {
                    } else {
                    }
                } else {
                }
            }
        });
    }

}