package info.appteve.lavradio.fragments;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import info.appteve.lavradio.R;
import info.appteve.lavradio.activities.HomeActivity;
import info.appteve.lavradio.adapters.offline.OfflineAdapter;
import info.appteve.lavradio.endpoints.EndpointKeys;
import info.appteve.lavradio.fragments.base.BaseFragment;
import info.appteve.lavradio.pojo.eventbus.EventBusDeleteOfflinePodcast;
import info.appteve.lavradio.pojo.eventbus.EventBusOfflineClick;
import info.appteve.lavradio.pojo.eventbus.EventBusOfflinePlayPodcast;
import info.appteve.lavradio.pojo.eventbus.EventBusOfflineStopMedia;
import info.appteve.lavradio.pojo.eventbus.EventBusRefreshOffline;
import info.appteve.lavradio.pojo.eventbus.EventBusStoragePermissionResult;
import info.appteve.lavradio.pojo.models.offlien_models.OfflineModel;
import info.appteve.lavradio.utils.FontUtils;

public class OfflineFragment extends BaseFragment {

    //    Android fields....
    @BindView(R.id.recyclerViewOffline)
    RecyclerView recyclerViewOffline;
    private Unbinder unbinder;
    @BindView(R.id.textViewOfflineHeading)
    TextView textViewOffline;

    //    Instance fields....
    List<OfflineModel> offlineModelList = new ArrayList<>();

    //    Adapter fields....
    private OfflineAdapter offlineAdapter;

    //    Font fields....
    private FontUtils fontUtils;

    public OfflineFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_offline, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    public void init() {
//        Registering event bus....
        EventBus.getDefault().register(this);

//        Setting layout manager....
        recyclerViewOffline.setLayoutManager(new LinearLayoutManager(getContext()));

//        Setting item animator....
        recyclerViewOffline.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOffline.getItemAnimator().setAddDuration(500);

//        Setting empty adapter....
        offlineAdapter = new OfflineAdapter(offlineModelList, getActivity());
        recyclerViewOffline.setAdapter(offlineAdapter);

//        Setting custom font....
        fontUtils = FontUtils.getFontUtils(getActivity());
        fontUtils.setTextViewBoldFont(textViewOffline);

        if (((HomeActivity) getActivity()).checkStoragePermission()) {
            getOfflineList();
        } else {
            ((HomeActivity) getActivity()).requestStoragePermission(101);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusPermissionResult(EventBusStoragePermissionResult eventBusStoragePermissionResult) {
        if (eventBusStoragePermissionResult.getRequestCode() == 101) {
            getOfflineList();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusDeleteOfflinePodCast(EventBusDeleteOfflinePodcast eventBusDeleteOfflinePodcast) {
        offlineModelList.get(eventBusDeleteOfflinePodcast.getPosition());
        File file = new File(offlineModelList.get(eventBusDeleteOfflinePodcast.getPosition()).getUrl());
        boolean deleted = file.delete();
        if (deleted = true) {
            offlineModelList.remove(offlineModelList.get(eventBusDeleteOfflinePodcast.getPosition()));
//            offlineAdapter.notifyItemRemoved(deleted);
            offlineAdapter.notifyDataSetChanged();
            Toast.makeText(getContext(), "deleted", Toast.LENGTH_SHORT).show();

        } else {

            Toast.makeText(getContext(), "not deleted", Toast.LENGTH_SHORT).show();

        }


    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusOfflineClick(final EventBusOfflineClick eventBusOfflineClick) {
        ((HomeActivity) getActivity()).selectedTab(EndpointKeys.OFFLINE_PODCAST_PLAYER);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().post(new EventBusOfflinePlayPodcast(eventBusOfflineClick.getPosition(), offlineModelList));
                EventBus.getDefault().post(new EventBusOfflineStopMedia(true));
            }
        }, 100);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshOffline(EventBusRefreshOffline eventBusRefreshOffline) {
        if (((HomeActivity) getActivity()).checkStoragePermission()) {
            getOfflineList();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        unbinder.unbind();
    }

    public void getOfflineList() {

        File radioFile = new File(Environment.getExternalStorageDirectory() + "/FennixRadio");
        File imagesFile = new File(radioFile + "/images");
        offlineAdapter.notifyItemRangeRemoved(0, offlineModelList.size());
        offlineModelList.clear();
        File[] contents = radioFile.listFiles();
        File[] imagescontents = imagesFile.listFiles();
        if (contents == null) {

        } else if (contents.length == 0) {

        } else {

            if (radioFile.isDirectory()) {

                for (File f : radioFile.listFiles()) {
                    if (f.isFile()) {
                        String fileName = f.getName();
                        String date = new Date(f.lastModified()).toString();
                        String fileUrl = radioFile + File.separator + fileName;
                        if (imagescontents == null) {
                            offlineModelList.add(new OfflineModel(fileName, date, fileUrl, ""));
                        } else if (imagescontents.length == 0) {
                            offlineModelList.add(new OfflineModel(fileName, date, fileUrl, ""));
                        } else {
                            if (imagesFile.isDirectory()) {
                                for (File file : imagesFile.listFiles()) {
                                    if (file.isFile()) {
                                        if (fileName.substring(0, fileName.indexOf(".")).contains(file.getName().substring(0, file.getName().indexOf(".")))) {
                                            String imageUrl = imagesFile + File.separator + file.getName();
                                            offlineModelList.add(new OfflineModel(fileName, date, fileUrl, imageUrl));
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        offlineAdapter.notifyItemInserted(offlineModelList.size() - 1);
                    }
//                    makeDraw();
                }
            } else {
                int duration = Toast.LENGTH_SHORT;
                Toast toast = Toast.makeText(getActivity(), getResources().getString(R.string.error_podcast), duration);
                toast.show();
            }

        }

    }
}
