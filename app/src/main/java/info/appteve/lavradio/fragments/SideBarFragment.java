package info.appteve.lavradio.fragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import info.appteve.lavradio.R;
import info.appteve.lavradio.adapters.sidebar.NavigationAdapter;
import info.appteve.lavradio.pojo.models.sidebar_models.SideBarModels;

public class SideBarFragment extends Fragment {

    //    Android fields....
    @BindView(R.id.recyclerViewNavigation)
    RecyclerView recyclerViewNavigation;
    @BindView(R.id.imageViewLogoNavigation)
    ImageView imageViewLogo;

    //    Adapter fields....
    private NavigationAdapter navigationAdapter;

    //    Instance fields....
    private List<SideBarModels> sideBarModelsList = new ArrayList<>();
    @BindString(R.string.radio)
    String radio;
    @BindString(R.string.offline)
    String offline;
    @BindString(R.string.nosotros)
    String nosotros;

    public SideBarFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_side_bar, container, false);
        ButterKnife.bind(this, view);

//        Setting layout manager....
        recyclerViewNavigation.setLayoutManager(new LinearLayoutManager(getActivity()));

//        Creating sidebar data....
        createSidebarData();

//        Setting adapter....
        navigationAdapter = new NavigationAdapter(getActivity(), sideBarModelsList);
        recyclerViewNavigation.setAdapter(navigationAdapter);

        Glide.with(getActivity()).load(R.drawable.logo).into(imageViewLogo);

        return view;
    }

    private void createSidebarData() {
        sideBarModelsList.add(new SideBarModels(R.drawable.icon_micro_phone, radio));
        sideBarModelsList.add(new SideBarModels(R.drawable.icon_offline, offline));
        sideBarModelsList.add(new SideBarModels(R.drawable.icon_info, nosotros));
    }

}
