package info.appteve.lavradio.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.Progress;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import info.appteve.lavradio.R;
import info.appteve.lavradio.activities.HomeActivity;
import info.appteve.lavradio.endpoints.EndpointUrl;
import info.appteve.lavradio.pojo.eventbus.EventBusPlayPodCast;
import info.appteve.lavradio.pojo.eventbus.EventBusRefreshOffline;
import info.appteve.lavradio.pojo.eventbus.EventBusStoragePermissionResult;
import info.appteve.lavradio.pojo.models.radio_models.PodCastModel;
import info.appteve.lavradio.radio.RadioService;
import info.appteve.lavradio.utils.FontUtils;

import static android.content.Context.POWER_SERVICE;
import static android.content.Context.TELEPHONY_SERVICE;

public class PodcastPlayerFragment extends Fragment {

    private static final String TAG = PodcastPlayerFragment.class.getSimpleName();
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;

    //    Android fields....
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewPodCast)
    TextView textViewPodCast;
    @BindView(R.id.textViewPodcastName)
    TextView textViewPodcastName;
    @BindView(R.id.imageViewPodcast)
    RoundedImageView roundedImageViewPodcast;
    @BindView(R.id.imageViewPlayPause)
    ImageView imageViewPlayPause;
    @BindView(R.id.imageViewPrevious)
    ImageView imageViewPrevious;
    @BindView(R.id.imageViewNext)
    ImageView imageViewNext;
    @BindView(R.id.seekBarPlayer)
    SeekBar seekBarPlayer;
    @BindView(R.id.textViewCurrentTime)
    TextView textViewCurrentTime;
    @BindView(R.id.textViewtotalTime)
    TextView textViewTotalTime;
    public MediaPlayer mediaPlayer;
    private int duration;
    ProgressDialog dialogz;
    private Unbinder unbinder;

    //    Font fields....
    private FontUtils fontUtils;

    //    Instance fields....
    private PodCastModel currentPodCast;
    private String urls;
    private boolean isMediaPlaying;
    private int currentPodCastPosition;
    private List<PodCastModel> podCastModelList = new ArrayList<>();
    int downloadIdOne;

    public PodcastPlayerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_podcast_player, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        Intent intent = new Intent(getActivity(), RadioService.class);
        getActivity().stopService(intent);

//        Registering event bus....
        EventBus.getDefault().register(this);

//        Setting custom font....
        fontUtils = FontUtils.getFontUtils(getActivity());
        fontUtils.setTextViewBoldFont(textViewPodCast);
        fontUtils.setTextViewBoldFont(textViewPodcastName);
        fontUtils.setTextViewRegularFont(textViewTotalTime);
        fontUtils.setTextViewRegularFont(textViewCurrentTime);

        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    if (mediaPlayer != null)
                        mediaPlayer.pause();
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager mgr = (TelephonyManager) getActivity().getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
        if (((HomeActivity) getActivity()).checkStoragePermission()) {
            createDirectory();
        } else {
            ((HomeActivity) getActivity()).requestStoragePermission(EXTERNAL_STORAGE_PERMISSION_CONSTANT);
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            if (mediaPlayer != null) {
                imageViewPlayPause.setImageResource(R.drawable.ic_action_play);
                isMediaPlaying = false;
                mediaPlayer.pause();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusPermissionResult(EventBusStoragePermissionResult eventBusStoragePermissionResult) {
        if (eventBusStoragePermissionResult.getRequestCode() == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            createDirectory();
        }
    }

    private boolean createDirectory() {
        File folder = new File(Environment.getExternalStorageDirectory(), "FennixRadio");
        boolean success = true;
        if (!folder.exists()) {
            success = folder.mkdir();
        }
        return success;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        unbinder.unbind();
    }

    @OnClick({R.id.imageViewBack, R.id.textViewPodCast})
    public void onBackClick(View view) {
        ((HomeActivity) getActivity()).onBackPressed();
    }

    @OnClick(R.id.imageViewPlayPause)
    public void onPlayPauseClick(View view) {
        if (isMediaPlaying) {
            imageViewPlayPause.setImageResource(R.drawable.ic_action_play);
            isMediaPlaying = false;
            if (mediaPlayer != null) {
                mediaPlayer.pause();
            }
        } else {
            imageViewPlayPause.setImageResource(R.drawable.ic_action_pause);
            isMediaPlaying = true;
            if (mediaPlayer != null) {
                mediaPlayer.start();
            }
        }
    }

    @OnClick(R.id.imageViewNext)
    public void onNextClick(View view) {
        nextTrack();
    }

    @OnClick(R.id.imageViewPrevious)
    public void onPreviousClick(View view) {
        forward();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusPlayPodCast(EventBusPlayPodCast eventBusPlayPodCast) {
        currentPodCastPosition = eventBusPlayPodCast.getPosition();
        podCastModelList.clear();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
            imageViewPlayPause.setImageResource(R.drawable.ic_action_play);
            isMediaPlaying = false;
            seekBarPlayer.setProgress(0);
            textViewCurrentTime.setText("00:00");
            textViewTotalTime.setText("00:00");
        }
        podCastModelList.addAll(eventBusPlayPodCast.getPodCastModelList());
        currentPodCast = podCastModelList.get(currentPodCastPosition);
        if (currentPodCast != null) {
            createPlayer(currentPodCast.getTrackFile(), currentPodCast.getTrackName());
        }
    }

    private void createPlayer(String url, String trackname) {
        urls = url.replaceAll(" ", "%20");
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(urls);
            mediaPlayer.prepareAsync();

            final ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Prepare to play. Please wait.");
            dialog.setCancelable(false);
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            dialog.show();

            textViewPodcastName.setText(trackname);
            Glide.with(getActivity())
                    .load(EndpointUrl.IMAGE_BASE_URL + currentPodCast.getImageFile())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                    .apply(new RequestOptions().centerCrop())
                    .into(roundedImageViewPodcast);

            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(final MediaPlayer mp) {

                    mp.start();
                    isMediaPlaying = true;

                    imageViewPlayPause.setImageResource(R.drawable.ic_action_pause);

                    mRunnable.run();
                    dialog.dismiss();
                    duration = mediaPlayer.getDuration();

                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            continiousPlay();
                        }
                    });
                }
            });

        } catch (IOException e) {
            Toast.makeText(getActivity(), getString(R.string.file_not_found), Toast.LENGTH_SHORT).show();
        }

    }

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            if (mediaPlayer != null && getActivity() != null) {

                seekBarPlayer.setMax(duration);

                textViewTotalTime.setText(getTimeString(duration));

                int mCurrentPosition = mediaPlayer.getCurrentPosition();
                seekBarPlayer.setProgress(mCurrentPosition);

                textViewCurrentTime.setText(getTimeString(mCurrentPosition));

                seekBarPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (mediaPlayer != null && fromUser) {
                            mediaPlayer.seekTo(progress);
                        }
                    }
                });


            }

            mHandler.postDelayed(this, 10);
        }
    };

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        long hours = millis / (1000 * 60 * 60);
        long minutes = (millis % (1000 * 60 * 60)) / (1000 * 60);
        long seconds = ((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000;

        buf.append(String.format("%02d", hours))
                .append(":")
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    public void continiousPlay() {
        if (currentPodCastPosition == podCastModelList.size() - 1) {
            currentPodCastPosition = 0;
        } else {
            currentPodCastPosition = currentPodCastPosition + 1;
        }
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        currentPodCast = podCastModelList.get(currentPodCastPosition);
        createPlayer(currentPodCast.getTrackFile(), currentPodCast.getTrackName());
    }

    public void nextTrack() {
        if (currentPodCastPosition == podCastModelList.size() - 1) {
            currentPodCastPosition = 0;
        } else {
            currentPodCastPosition = currentPodCastPosition + 1;
        }
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        currentPodCast = podCastModelList.get(currentPodCastPosition);
        createPlayer(currentPodCast.getTrackFile(), currentPodCast.getTrackName());
    }

    public void forward() {
        if (currentPodCastPosition == 0) {
            currentPodCastPosition = podCastModelList.size() - 1;
        } else {
            currentPodCastPosition = currentPodCastPosition - 1;
        }
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        currentPodCast = podCastModelList.get(currentPodCastPosition);
        createPlayer(currentPodCast.getTrackFile(), currentPodCast.getTrackName());
    }

    @OnClick(R.id.imageViewDownloadPodCast)
    public void onViewClicked() {
        if (((HomeActivity) getActivity()).checkStoragePermission()) {
            if (createDirectory()) {
                downloadMediaFile(currentPodCast.getTrackFile(), currentPodCast.getTrackName());
                downloadThumbNail(EndpointUrl.IMAGE_BASE_URL + "" + currentPodCast.getImageFile(), currentPodCast.getTrackName());
            }
        } else {
            ((HomeActivity) getActivity()).requestStoragePermission(EXTERNAL_STORAGE_PERMISSION_CONSTANT);
        }
    }

    public void downloadThumbNail(String url, String names) {
        url = url.replaceAll(" ", "%20");
        // Setting timeout globally for the download network requests:
        downloadIdOne = PRDownloader.download(url, Environment.getExternalStorageDirectory() +
                File.separator + "FennixRadio/images", names + ".jpg")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
//                        Log.i("Progress", "Start");
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
//                        Log.i("Progress", "Paused");
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
//                        Log.i("Progress", "Canceled");
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
//                        Log.i("Progress", progress.currentBytes + " " + progress.totalBytes + " " + progress.toString());
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {

                    }

                    @Override
                    public void onError(Error error) {
                        Toast.makeText(getActivity(), "Error Downloading File", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void downloadMediaFile(String url, String names) {
        url = url.replaceAll(" ", "%20");
        dialogz = new ProgressDialog(getActivity());
        dialogz.setMessage("Track: " + names + " ,downloaded....");
        dialogz.setCancelable(false);
//        dialogz.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialogz.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        dialogz.show();

        // Setting timeout globally for the download network requests:
        downloadIdOne = PRDownloader.download(url, Environment.getExternalStorageDirectory() +
                File.separator + "FennixRadio/", names + ".mp3")
                .build()
                .setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
//                        Log.i("Progress", "Start");
                    }
                })
                .setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {
//                        Log.i("Progress", "Paused");
                    }
                })
                .setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
//                        Log.i("Progress", "Canceled");
                    }
                })
                .setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {
//                        Log.i("Progress", progress.currentBytes + " " + progress.totalBytes + " " + progress.toString());
                    }
                })
                .start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        dialogz.dismiss();
                        EventBus.getDefault().post(new EventBusRefreshOffline());
                        Toast.makeText(getActivity(), "File downloaded successfully.", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onError(Error error) {
                        Toast.makeText(getActivity(), "Error Downloading File", Toast.LENGTH_SHORT).show();
                    }
                });
    }

}