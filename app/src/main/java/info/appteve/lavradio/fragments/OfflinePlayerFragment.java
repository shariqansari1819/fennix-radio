package info.appteve.lavradio.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import info.appteve.lavradio.R;
import info.appteve.lavradio.activities.HomeActivity;
import info.appteve.lavradio.pojo.eventbus.EventBusOfflinePlayPodcast;
import info.appteve.lavradio.pojo.models.offlien_models.OfflineModel;
import info.appteve.lavradio.radio.RadioService;
import info.appteve.lavradio.utils.FontUtils;

import static android.content.Context.TELEPHONY_SERVICE;

public class OfflinePlayerFragment extends Fragment {

    private static final String TAG = OfflinePlayerFragment.class.getSimpleName();
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;

    //    Android fields....
    @BindView(R.id.imageViewBack)
    ImageView imageViewBack;
    @BindView(R.id.textViewPodCast)
    TextView textViewPodCast;
    @BindView(R.id.textViewPodcastName)
    TextView textViewPodcastName;
    @BindView(R.id.imageViewPodcast)
    RoundedImageView roundedImageViewPodcast;
    @BindView(R.id.imageViewPlayPause)
    ImageView imageViewPlayPause;
    @BindView(R.id.imageViewPrevious)
    ImageView imageViewPrevious;
    @BindView(R.id.imageViewNext)
    ImageView imageViewNext;
    @BindView(R.id.seekBarPlayer)
    SeekBar seekBarPlayer;
    @BindView(R.id.textViewCurrentTime)
    TextView textViewCurrentTime;
    @BindView(R.id.textViewtotalTime)
    TextView textViewTotalTime;
    public MediaPlayer mediaPlayer;
    private int duration;
    ProgressBar progressBar;
    ProgressDialog dialogz;
    private Unbinder unbinder;

    //    Font fields....
    private FontUtils fontUtils;

    //    Instance fields....
    private OfflineModel offlineModel;
    private String urls;
    private boolean isMediaPlaying;
    private int currentOfflinePosition;
    private List<OfflineModel> offlineModelList = new ArrayList<>();

    public OfflinePlayerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_offline_player, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Intent intent = new Intent(getActivity(), RadioService.class);
        getActivity().stopService(intent);

        EventBus.getDefault().register(this);

        //        Setting custom font....
        fontUtils = FontUtils.getFontUtils(getActivity());
        fontUtils.setTextViewBoldFont(textViewPodCast);
        fontUtils.setTextViewBoldFont(textViewPodcastName);
        fontUtils.setTextViewRegularFont(textViewTotalTime);
        fontUtils.setTextViewRegularFont(textViewCurrentTime);

        PhoneStateListener phoneStateListener = new PhoneStateListener() {
            @Override
            public void onCallStateChanged(int state, String incomingNumber) {
                if (state == TelephonyManager.CALL_STATE_RINGING) {
                    if (mediaPlayer != null)
                        mediaPlayer.pause();
                }
                super.onCallStateChanged(state, incomingNumber);
            }
        };
        TelephonyManager mgr = (TelephonyManager) getActivity().getSystemService(TELEPHONY_SERVICE);
        if (mgr != null) {
            mgr.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        unbinder.unbind();
    }

    @OnClick({R.id.imageViewBack, R.id.textViewPodCast})
    public void onBackClick(View view) {
        ((HomeActivity) getActivity()).onBackPressed();
    }

    @OnClick(R.id.imageViewPlayPause)
    public void onPlayPauseClick(View view) {
        if (isMediaPlaying) {
            imageViewPlayPause.setImageResource(R.drawable.ic_action_play);
            isMediaPlaying = false;
            if (mediaPlayer != null) {
                mediaPlayer.pause();
            }
        } else {
            imageViewPlayPause.setImageResource(R.drawable.ic_action_pause);
            isMediaPlaying = true;
            if (mediaPlayer != null) {
                mediaPlayer.start();
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            if (mediaPlayer != null) {
                imageViewPlayPause.setImageResource(R.drawable.ic_action_play);
                isMediaPlaying = false;
                mediaPlayer.pause();
            }
        }
    }

    @OnClick(R.id.imageViewNext)
    public void onNextClick(View view) {
        nextTrack();
    }

    @OnClick(R.id.imageViewPrevious)
    public void onPreviousClick(View view) {
        forward();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusOfflineModel(EventBusOfflinePlayPodcast eventBusOfflinePlayPodcast) {
        currentOfflinePosition = eventBusOfflinePlayPodcast.getPosition();
        offlineModelList.clear();
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
            imageViewPlayPause.setImageResource(R.drawable.ic_action_play);
            isMediaPlaying = false;
            seekBarPlayer.setProgress(0);
            textViewCurrentTime.setText("00:00");
            textViewTotalTime.setText("00:00");
        }
        offlineModelList.addAll(eventBusOfflinePlayPodcast.getOfflineModelList());
        offlineModel = offlineModelList.get(currentOfflinePosition);
        if (offlineModel != null) {
            createPlayer(offlineModel.getUrl(), offlineModel.getName(), offlineModel.getImageUrl());
        }
    }

    private void createPlayer(String url, String trackname, String imageUrl) {
//        urls = url.replaceAll(" ", "%20");
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepareAsync();

//            final ProgressDialog dialog = new ProgressDialog(getActivity());
//            dialog.setMessage("Prepare to play. Please wait.");
//            dialog.setCancelable(false);
//            dialog.getWindow().setGravity(Gravity.CENTER);
//            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//            dialog.show();

            String str_track = trackname.replaceFirst("\\.mp3$", "");

            textViewPodcastName.setText(str_track);
            Glide.with(getActivity()).
                    load(imageUrl).apply(new RequestOptions().placeholder(R.drawable.placeholder)).into(roundedImageViewPodcast);
//            Glide.with(getActivity())
//                    .load(EndpointUrl.IMAGE_BASE_URL + offlineModel.getImageFile())
//                    .apply(new RequestOptions().placeholder(R.drawable.placeholder))
//                    .apply(new RequestOptions().centerCrop())
//                    .into(roundedImageViewPodcast);

            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
//                    dialog.dismiss();
                    Toast.makeText(getActivity(), what + " " + extra, Toast.LENGTH_SHORT).show();
                    return false;
                }
            });

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(final MediaPlayer mp) {

                    mp.start();
                    isMediaPlaying = true;

                    imageViewPlayPause.setImageResource(R.drawable.ic_action_pause);

                    mRunnable.run();
//                    dialog.dismiss();
                    duration = mediaPlayer.getDuration();

                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        public void onCompletion(MediaPlayer mp) {
                            continiousPlay();
                        }
                    });
                }
            });

        } catch (IOException e) {
            Toast.makeText(getActivity(), getString(R.string.file_not_found), Toast.LENGTH_SHORT).show();
        }

    }

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            if (mediaPlayer != null && getActivity() != null) {

                seekBarPlayer.setMax(duration);

                textViewTotalTime.setText(getTimeString(duration));

                int mCurrentPosition = mediaPlayer.getCurrentPosition();
                seekBarPlayer.setProgress(mCurrentPosition);

                textViewCurrentTime.setText(getTimeString(mCurrentPosition));

                seekBarPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (mediaPlayer != null && fromUser) {
                            mediaPlayer.seekTo(progress);
                        }
                    }
                });


            }

            mHandler.postDelayed(this, 10);
        }
    };

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        long hours = millis / (1000 * 60 * 60);
        long minutes = (millis % (1000 * 60 * 60)) / (1000 * 60);
        long seconds = ((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000;

        buf.append(String.format("%02d", hours))
                .append(":")
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }

    public void continiousPlay() {
        if (currentOfflinePosition == offlineModelList.size() - 1) {
            currentOfflinePosition = 0;
        } else {
            currentOfflinePosition = currentOfflinePosition + 1;
        }
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        offlineModel = offlineModelList.get(currentOfflinePosition);
        createPlayer(offlineModel.getUrl(), offlineModel.getName(), offlineModel.getImageUrl());
    }

    public void nextTrack() {
        if (currentOfflinePosition == offlineModelList.size() - 1) {
            currentOfflinePosition = 0;
        } else {
            currentOfflinePosition = currentOfflinePosition + 1;
        }
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        offlineModel = offlineModelList.get(currentOfflinePosition);
        createPlayer(offlineModel.getUrl(), offlineModel.getName(), offlineModel.getImageUrl());
    }

    public void forward() {
        if (currentOfflinePosition == 0) {
            currentOfflinePosition = offlineModelList.size() - 1;
        } else {
            currentOfflinePosition = currentOfflinePosition - 1;
        }
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        offlineModel = offlineModelList.get(currentOfflinePosition);
        createPlayer(offlineModel.getUrl(), offlineModel.getName(), offlineModel.getImageUrl());
    }

}
