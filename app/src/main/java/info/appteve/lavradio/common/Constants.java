package info.appteve.lavradio.common;

public interface Constants {
    String DATA_KEY_1 = ".data_key_1";
    String DATA_KEY_2 = ".data_key_2";

    String FEATURED_IMAGE = "featured_image";
    String RADIO_IMAGE = "radio_image";
}
