package info.appteve.lavradio.radio;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

import info.appteve.lavradio.R;
import info.appteve.lavradio.RadioInfoHelper;
import info.appteve.lavradio.Utilites;
import info.appteve.lavradio.constants.Constants;
import info.appteve.lavradio.pojo.eventbus.EventBusPlayerStatus;
import info.appteve.lavradio.pojo.eventbus.EventBusRadioName;


public class RadioService extends Service implements Player.EventListener, AudioManager.OnAudioFocusChangeListener, Constants {


    // public RadioStation mCurrentStation;

    //  public RadioStation getCurrentStation() {
    //  return mCurrentStation;
    // }

    public static final String ACTION_PLAY = "com.appteve.transistorone.radio.ACTION_PLAY";
    public static final String ACTION_PAUSE = "com.appteve.transistorone.radio.ACTION_PAUSE";
    public static final String ACTION_STOP = "com.appteve.transistorone.radio.ACTION_STOP";

    private final IBinder iBinder = new LocalBinder();

    private Handler handler;
    private SimpleExoPlayer exoPlayer;
    private MediaSessionCompat mediaSession;
    private MediaControllerCompat.TransportControls transportControls;

    private boolean onGoingCall = false;
    private TelephonyManager telephonyManager;

    private WifiManager.WifiLock wifiLock;

    private AudioManager audioManager;

    private MediaNotificationManager notificationManager;

    private boolean serviceInUse = false;

    private String status;

    private String strAppName;
    private String strLiveBroadcast;
    private String streamUrl;
    static String radioName;

    IcyStreamMeta streamMeta;
    MetadataTask2 metadataTask2;

    String title_artist;
    Timer timer;
    Bitmap cov;
    int timer_time = 1000;

    SharedPreferences sharedPreferences;
    SharedPreferences sharedPreferences1;

    final static BitmapFactory.Options options = new BitmapFactory.Options(); //

    static String urlz;
    static String artz;
    static String rd;
    int c;

   /* @Override
    public void onClickItem(RadioStation station, int code) {
        Log.d("kek", "clicked in radioservice.java");
        mCurrentStation = station;
        notificationManager.startNotify(status);
    } */

    public class LocalBinder extends Binder {
        public RadioService getService() {
            return RadioService.this;
        }
    }

    private BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            pause();
        }

    };

    private PhoneStateListener phoneStateListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (state == TelephonyManager.CALL_STATE_OFFHOOK
                    || state == TelephonyManager.CALL_STATE_RINGING) {

                if (!isPlaying()) return;

                onGoingCall = true;
                stop();

            } else if (state == TelephonyManager.CALL_STATE_IDLE) {

                if (!onGoingCall) return;

                onGoingCall = false;
                resume();

            }
        }

    };

    private MediaSessionCompat.Callback mediasSessionCallback = new MediaSessionCompat.Callback() {
        @Override
        public void onPause() {
            super.onPause();

            pause();
        }

        @Override
        public void onStop() {
            super.onStop();

            stop();
            //  notificationManager.cancelNotify();
        }

        @Override
        public void onPlay() {
            super.onPlay();
            // startData();

            resume();
        }
    };

    public void getPostListen(int isActive, int listenTime) {
        RadioInfoHelper.postListenStatus(isActive, listenTime, new RadioInfoHelper.PostListenCallback() {
            @Override
            public void finishPostListen(JSONObject jsonObject) {
                try {
                    if (jsonObject != null) {

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(),
                            "Errwor: " + e.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        }, RadioService.this);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        serviceInUse = true;

        return iBinder;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("Service", "OnCreate");

        strAppName = getResources().getString(R.string.app_name);
        strLiveBroadcast = getResources().getString(R.string.live_broadcast);

        onGoingCall = false;

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        notificationManager = new MediaNotificationManager(this);


        wifiLock = ((WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mcScPAmpLock");

        mediaSession = new MediaSessionCompat(this, getClass().getSimpleName());
        transportControls = mediaSession.getController().getTransportControls();
        mediaSession.setActive(true);
        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setMetadata(new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, "...")
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, strAppName)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, strLiveBroadcast)
                .build());
        mediaSession.setCallback(mediasSessionCallback);

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        handler = new Handler();
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
        DefaultTrackSelector trackSelector = new DefaultTrackSelector(trackSelectionFactory);
        exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);
        exoPlayer.addListener(this);

        registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

        status = PlaybackStatus.IDLE;
        sharedPreferences = getSharedPreferences("service", Context.MODE_PRIVATE);
        sharedPreferences1 = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        radioName = sharedPreferences1.getString(APP_PREFERENCES_NAME_STATION, "1");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("Service", "OnStartCommand");

        String action = intent.getAction();

        if (TextUtils.isEmpty(action))
            return START_NOT_STICKY;

        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            stop();
            return START_NOT_STICKY;
        }
        if (action.equalsIgnoreCase(ACTION_PLAY)) {

            transportControls.play();
            //  startData(streamUrl);

        } else if (action.equalsIgnoreCase(ACTION_PAUSE)) {
            transportControls.pause();
            stopData();
            c = 0;
            sharedPreferences.edit().putString("as", "0").apply();


        } else if (action.equalsIgnoreCase(ACTION_STOP)) {
            transportControls.stop();
            notificationManager.cancelNotify();
            stopData();

        }

        return START_NOT_STICKY;
    }


    @Override
    public boolean onUnbind(Intent intent) {
        serviceInUse = false;
        Log.d("kek3", "in onUnbind");

        if (status.equals(PlaybackStatus.IDLE))
            stopSelf();

        return true;
    }

    @Override
    public void onRebind(final Intent intent) {
        serviceInUse = true;
    }

    @Override
    public void onDestroy() {
        Log.i("Service", "OnDestroy");

        pause();

        exoPlayer.release();
        exoPlayer.removeListener(this);

        if (telephonyManager != null)
            telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);

        notificationManager.cancelNotify();

        mediaSession.release();

        unregisterReceiver(becomingNoisyReceiver);

        super.onDestroy();

    }

    @Override
    public void onAudioFocusChange(int focusChange) {

        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:

                exoPlayer.setVolume(0.8f);

                resume();

                break;

            case AudioManager.AUDIOFOCUS_LOSS:

                stop();

                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:

                if (isPlaying()) pause();

                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:

                if (isPlaying())
                    exoPlayer.setVolume(0.1f);

                break;
        }

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case Player.STATE_BUFFERING:
                status = PlaybackStatus.LOADING;
                Log.i("Player", "Buffering");
                break;
            case Player.STATE_ENDED:
                status = PlaybackStatus.STOPPED;
                Log.i("Player", "Stopped");
                break;
            case Player.STATE_IDLE:
                status = PlaybackStatus.IDLE;
                EventBus.getDefault().post(new EventBusPlayerStatus("stopped"));
                getPostListen(0, getCurrentProgress());
                break;
            case Player.STATE_READY:
                status = playWhenReady ? PlaybackStatus.PLAYING : PlaybackStatus.PAUSED;
                if (status.equals(PlaybackStatus.PLAYING)) {
                    Log.i("Player", "Playing");
                    getPostListen(1, 0);
                    EventBus.getDefault().post(new EventBusPlayerStatus("playing"));
                } else {
                    Log.i("Player", "Paused");
                    EventBus.getDefault().post(new EventBusPlayerStatus("paused"));
                    getPostListen(0, getCurrentProgress());
                }
                break;
            default:
                status = PlaybackStatus.IDLE;
                break;
        }

        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(),
                R.drawable.defimage);

        if (!status.equals(PlaybackStatus.IDLE))


            notificationManager.startNotify(status, radioName, bitmap, "");

        EventBus.getDefault().post(status);
    }


    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

        EventBus.getDefault().post(PlaybackStatus.ERROR);

    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }

    public void play(String streamUrl) {
        Log.i("Service", "Play");
        this.streamUrl = streamUrl;

        if (wifiLock != null && !wifiLock.isHeld()) {

            wifiLock.acquire();

        }

        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        DefaultDataSourceFactory dataSourceFactory = new DefaultDataSourceFactory(this, Util.getUserAgent(this, getClass().getSimpleName()), bandwidthMeter);
        DefaultExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
        ExtractorMediaSource mediaSource = new ExtractorMediaSource(Uri.parse(streamUrl), dataSourceFactory, extractorsFactory, handler, null);

        exoPlayer.prepare(mediaSource);
        exoPlayer.setPlayWhenReady(true);

        startData(streamUrl);


    }


    public void resume() {

        if (!isPlaying()) {

            if (streamUrl != null)
                play(streamUrl);
        }

    }

    public void pause() {

        exoPlayer.setPlayWhenReady(false);

        audioManager.abandonAudioFocus(this);
        wifiLockRelease();
    }

    public void stop() {

        exoPlayer.stop();

        audioManager.abandonAudioFocus(this);
        wifiLockRelease();
    }

    public void playOrPause(String url, String radioNamez) {


        if (streamUrl != null && streamUrl.equals(url)) {

            if (!isPlaying()) {

                radioName = radioNamez;

                play(streamUrl);
                //  startData(url);
                c = 0;

            } else {

                pause();

                stopData();
                sharedPreferences.edit().putString("as", "0").apply();

            }

        } else {
            radioName = radioNamez;
            if (isPlaying()) {

                pause();

            }

            play(url);

        }

    }

    public String getStatus() {

        return status;

    }

    public MediaSessionCompat getMediaSession() {

        return mediaSession;
    }

    public boolean isPlaying() {

        return this.status.equals(PlaybackStatus.PLAYING);
    }

    public int getCurrentProgress() {
        return (int) exoPlayer.getCurrentPosition() / 1000;
    }

    private void wifiLockRelease() {

        if (wifiLock != null && wifiLock.isHeld()) {

            wifiLock.release();

        }

    }

    public void stopTS() {
        transportControls.stop();
        notificationManager.cancelNotify();
    }

    public void updateNoty(String title, Bitmap covr, String rn) {

        System.out.println();

        notificationManager.startNotify(status, title, covr, rn);

    }

    //// image

    public void startData(String stream) {


        streamMeta = new IcyStreamMeta();
        try {
            streamMeta.setStreamUrl(new URL(stream));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        metadataTask2 = new MetadataTask2();
        try {
            metadataTask2.execute(new URL(stream));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        timer = new Timer();
        MyTimerTask task = new MyTimerTask();
        timer.schedule(task, 100, timer_time);
    }

    protected class MetadataTask2 extends AsyncTask<URL, Void, IcyStreamMeta> {
        @Override
        protected IcyStreamMeta doInBackground(URL... urls) {
            try {
                Log.e("Refresh", "Refreshing");
                streamMeta.refreshMeta();
                Log.e("Retrieving MetaData", "Refreshed Metadata");
            } catch (IOException e) {
                Log.e(MetadataTask2.class.toString(), e.getMessage());
            }
            return streamMeta;
        }

        @Override
        protected void onPostExecute(IcyStreamMeta result) {
            try {
                title_artist = streamMeta.getStreamTitle();

            } catch (IOException e) {
                Log.e(MetadataTask2.class.toString(), e.getMessage());
            }
        }
    }

    public void stopData() {

        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
            // start = false;
        }
    }

    class MyTimerTask extends TimerTask {
        public void run() {
            try {
                streamMeta.refreshMeta();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                final String title_artist = streamMeta.getStreamTitle();
                final String z = new String(title_artist.getBytes("ISO-8859-1"), "UTF-8");

                c = c + 1;
                if ((sharedPreferences.getString("as", "")).equals(z)) {
                    if (c < 4) {
                        udp(z);
                    }

                } else {
                    c = 0;
                    udp(z);
                }


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public void udp(String z) {


        getImageArtist(z, radioName, this);
        sharedPreferences.edit().putString("as", z).apply();
        sharedPreferences.edit().putString(APP_PREFERENCES_NAME_ARTIST, z).apply();


    }

    public static void getImageArtist(final String artist, final String radio, final Context context) {


        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {
                String queryURL = "https://itunes.apple.com/search?term=" + URLEncoder.encode(artist) + "&limit=1";

                JSONObject jsonObject = Utilites.getJSONObjectFromUrl(queryURL);

                artz = artist;
                rd = radio;

                try {

                    if (jsonObject != null) {

                        int counting = jsonObject.getInt("resultCount");
                        if (counting == 0) {
                            urlz = IMAGE_NOT_FOUND;
                        } else {

                            JSONArray resp = jsonObject.getJSONArray("results");

                            if (resp.length() == 0) {

                            } else {

                                JSONObject obj = resp.getJSONObject(0);

                                String urlImage = obj.getString("artworkUrl100");
                                urlImage = urlImage.replace("100x100bb", "300x300bb");
                                urlz = urlImage.replace(".jpg", ".png");
                            }


                        }

                        return urlz;
                    } else {
                        Log.v("INFO", "Cover not found");
                        return IMAGE_NOT_FOUND;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final String imageUrl) {

                Log.d("Data", imageUrl + "Is this....");
                if (!imageUrl.isEmpty()) {

                    Picasso.with(context)
                            .load(imageUrl)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {

                                    if (artz.isEmpty()) {
                                        artz = "Artist not found";
                                    }

                                    RadioManager.getService().updateNoty(artz, bitmap, radioName);
                                    Intent intent = new Intent("cover");
                                    intent.putExtra("message", encodeTobase64(bitmap));
                                    intent.putExtra("messagez", "cover");
                                    intent.putExtra("artist", artz);
                                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                                    EventBus.getDefault().post(new EventBusRadioName(artz));

                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });
                } else {


                    options.inJustDecodeBounds = true;
                    options.inSampleSize = 2;
                    options.inJustDecodeBounds = false;
                    options.inTempStorage = new byte[16 * 1024];

                    Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.defimage);
                    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bmp, 300, 300, false);
                    RadioManager.getService().updateNoty(artist, resizedBitmap, radioName);
                }
            }
        }.execute();


    }


    public static String encodeTobase64(Bitmap image) {
        Bitmap immage = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immage.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        // Log.d("Image Log:", imageEncoded);
        return imageEncoded;
    }
}
