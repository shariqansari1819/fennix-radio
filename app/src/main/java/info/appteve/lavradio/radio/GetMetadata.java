package info.appteve.lavradio.radio;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Timer;
import java.util.TimerTask;

public class GetMetadata {

    IcyStreamMeta streamMeta;
    MetadataTask metadataTask;
    String title_artist;
    Timer timer;

    public String startData(String urlStream){

        streamMeta = new IcyStreamMeta();
        try {
            streamMeta.setStreamUrl(new URL(urlStream));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        metadataTask =new MetadataTask();
        try {
            metadataTask.execute(new URL(urlStream));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

         timer = new Timer();
        MyTimerTask task = new MyTimerTask();
        timer.schedule(task,100, 10000);

        System.out.println("ZZZ - - - - - - "+title_artist);

        return title_artist;


    }

    public void stopData(){
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    protected class MetadataTask extends AsyncTask<URL, Void, IcyStreamMeta>
    {
        @Override
        protected IcyStreamMeta doInBackground(URL... urls)
        {
            try
            {
                streamMeta.refreshMeta();
                Log.e("Retrieving MetaData","Refreshed Metadata");
            }
            catch (IOException e)
            {
                Log.e(MetadataTask.class.toString(), e.getMessage());
            }
            return streamMeta;
        }

        @Override
        protected void onPostExecute(IcyStreamMeta result)
        {
            try
            {
                title_artist=streamMeta.getStreamTitle();
                Log.e("Retrieved title_artist", title_artist);
                if(title_artist.length()>0)
                {
                    //   textView.setText(title_artist);
                }
            }
            catch (IOException e)
            {
                Log.e(MetadataTask.class.toString(), e.getMessage());
            }
        }
    }

    class MyTimerTask extends TimerTask {

        private RadioService rr;
        public void run() {
            try {
                streamMeta.refreshMeta();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                String title_artist=streamMeta.getStreamTitle();
                //  String z = URLEncoder.encode(title_artist, "ascii");
                // String z1 = URLEncoder.encode(z, "utf-8");

               // System.out.println(isEncoded("CHECK - - - - - - - -" + title_artist));
                String z = new String(title_artist.getBytes("ISO-8859-1"), "UTF-8");










                Log.i("ARTIST TITLE", "\n "+"\n"+title_artist+ "\n MD: " +streamMeta.getMetadata()+ "\n LEGT : " + title_artist.length()+ "\nEBCODED: " + z);

                if (title_artist.length() > 0){

                    title_artist= z;





                    //  textView.setText(title_artist);

                } else {
                    // textView.setText("not");
                }
                // textView.setText(title_artist);
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    public boolean isEncoded(String text){

        Charset charset = Charset.forName("ISO-8859-1");
        String checked=new String(text.getBytes(charset),charset);
        return !checked.equals(text);

    }
}
