package info.appteve.lavradio.constants;

/**
 * Created by appteve on 22/04/16.
 */
public interface Constants {

    String APP_NAME = "Fennix Radio";
    String MENU_SITE_URL = "https://www.fennixpro.com";
    String GOOGLE_APP_URL = "https://play.google.com/store/apps/details?id=info.appteve.lavradio";
    String MENU_APPLICATIONS_URL = "https://play.google.com/store/apps/developer?id=APPTEVE%20DEV";
    String GENERAL_API_URL = "http://laekiss.fennixpro.com/";
    String IP_ADDRESS_API_URL = "http://lnq.demo.leadconcept.net";
    Boolean SHOW_ADS = false;


    /// DONT CHANGE THIS !!!!!!!!!!

    String UPLOADS_FOLDER = "uploads/";
    String MUSIC_FOLDER = "music/";
    String KEY_STRING = "&X-API-KEY=";
    String URL_API_GET_LISTRADIO = "endpoint/radio/getall/?id=";
    String URL_API_GET_NEWS = "endpoint/news/get?X-API-KEY=";
    String URL_API_GET_PODCAST = "endpoint/podcast/get?X-API-KEY=";
    String API_KEY = "testkey";
    String SDCARD_FOLDER = "/Castio/";
    String SDCARD_FOLDER_W = "Castio";
    String IMAGE_NOT_FOUND = "http://static.radio.net/images/broadcasts/43/08/34749/c300.png";
    String URL_FEATURED_IMAGES = "endpoint/News/featured/get?X-API-KEY=";
    String URL_IP_ADDRESS = "endpoint/get_api?X-API-KEY=";

    String APP_PREFERENCES = "radios";
    String APP_PREFERENCES_NAME_STATION = "station_name";
    String APP_PREFERENCES_NAME_ARTIST = "artist";
    String APP_PREFERENCES_NAME_IMAGE = "image";
    String APP_PREFERENCES_URL_STATION = "url";
    String APP_PREFERENCES_STATION_IMAGE = "image";
    String APP_PREFERENCES_BIG_IMAGE = "bigimage";


}
