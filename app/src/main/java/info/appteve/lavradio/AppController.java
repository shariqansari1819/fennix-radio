package info.appteve.lavradio;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.multidex.MultiDex;


import com.androidnetworking.AndroidNetworking;
import com.onesignal.OneSignal;


public class AppController extends Application {
    public static final String TAG = AppController.class
            .getSimpleName();

    SharedPreferences prefs = null;

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidNetworking.initialize(getApplicationContext());

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

//        FontsOverride.setDefaultFont(this, "DEFAULT", "appteve/Oswaldesque-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "MONOSPACE", "appteve/Oswaldesque-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "SERIF", "appteve/Oswaldesque-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "SANS_SERIF", "appteve/Oswaldesque-Regular.ttf");


        prefs = getSharedPreferences("info.com.castio", MODE_PRIVATE);

        if (prefs.getBoolean("firstrun", true)) {
            prefs.edit().putBoolean("firstrun", false).apply(); //prefs

        }
    }
}