package info.appteve.lavradio.pojo.models.radio_models;

public class FavouritesModel {

    private String name;
    private String URL;

    public FavouritesModel() {
    }

    public FavouritesModel(String name, String URL) {
        this.name = name;
        this.URL = URL;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
