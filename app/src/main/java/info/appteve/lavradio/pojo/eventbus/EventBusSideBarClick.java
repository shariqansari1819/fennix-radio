package info.appteve.lavradio.pojo.eventbus;

public class EventBusSideBarClick {

    private int position;

    public EventBusSideBarClick(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
