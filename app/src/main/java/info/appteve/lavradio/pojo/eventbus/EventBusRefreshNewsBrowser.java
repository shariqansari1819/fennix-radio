package info.appteve.lavradio.pojo.eventbus;

public class EventBusRefreshNewsBrowser {

    private String url;

    public EventBusRefreshNewsBrowser(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
