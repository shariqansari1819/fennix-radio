package info.appteve.lavradio.pojo.eventbus;

public class EventBusStoragePermissionResult {

    private int requestCode;

    public EventBusStoragePermissionResult(int requestCode) {
        this.requestCode = requestCode;
    }

    public int getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(int requestCode) {
        this.requestCode = requestCode;
    }
}
