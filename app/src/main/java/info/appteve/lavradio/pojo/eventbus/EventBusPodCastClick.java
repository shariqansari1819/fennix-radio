package info.appteve.lavradio.pojo.eventbus;

public class EventBusPodCastClick {

    private int position;

    public EventBusPodCastClick(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
