
package info.appteve.lavradio.pojo.models.radio_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PodCastModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("track_name")
    @Expose
    private String trackName;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("image_file")
    @Expose
    private String imageFile;
    @SerializedName("track_file")
    @Expose
    private String trackFile;
    @SerializedName("file")
    @Expose
    private String file;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTrackName() {
        return trackName;
    }

    public void setTrackName(String trackName) {
        this.trackName = trackName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public String getTrackFile() {
        return trackFile;
    }

    public void setTrackFile(String trackFile) {
        this.trackFile = trackFile;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

}
