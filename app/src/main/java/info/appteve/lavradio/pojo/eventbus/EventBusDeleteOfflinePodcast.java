package info.appteve.lavradio.pojo.eventbus;

public class EventBusDeleteOfflinePodcast {
    private int position;


    public EventBusDeleteOfflinePodcast(int position) {
        this.position = position;
    }
    public int getPosition() {
        return position;
    }
    public void setPosition(int position) {
        this.position = position;
    }
}
