package info.appteve.lavradio.pojo.eventbus;

public class EventBusNewsImageClick {

    private int position;

    public EventBusNewsImageClick(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
