package info.appteve.lavradio.pojo.eventbus;

public class EventBusOfflineStopMedia {

    private boolean Stop;

    public EventBusOfflineStopMedia(boolean stop) {
        Stop = stop;
    }

    public boolean isStop() {
        return Stop;
    }

    public void setStop(boolean stop) {
        Stop = stop;
    }
}
