
package info.appteve.lavradio.pojo.models.radio_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Preroll {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("radio_id")
    @Expose
    private String radioId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("date_added")
    @Expose
    private String dateAdded;
    @SerializedName("learn_more")
    @Expose
    private String learnMore;

    public String getLearnMore() {
        return learnMore;
    }

    public void setLearnMore(String learnMore) {
        this.learnMore = learnMore;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRadioId() {
        return radioId;
    }

    public void setRadioId(String radioId) {
        this.radioId = radioId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

}
