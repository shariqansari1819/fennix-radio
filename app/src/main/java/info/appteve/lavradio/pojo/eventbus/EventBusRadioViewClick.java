package info.appteve.lavradio.pojo.eventbus;

public class EventBusRadioViewClick {
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public EventBusRadioViewClick(int position) {
        this.position = position;
    }
}
