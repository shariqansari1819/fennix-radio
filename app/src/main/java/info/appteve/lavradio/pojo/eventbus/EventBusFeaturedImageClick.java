package info.appteve.lavradio.pojo.eventbus;

public class EventBusFeaturedImageClick {

    private int position;

    public EventBusFeaturedImageClick(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
