package info.appteve.lavradio.pojo.eventbus;

import java.util.List;

import info.appteve.lavradio.pojo.models.radio_models.PodCastModel;

public class EventBusPlayPodCast {

    private int position;
    private List<PodCastModel> podCastModelList;

    public EventBusPlayPodCast(int position, List<PodCastModel> podCastModelList) {
        this.position = position;
        this.podCastModelList = podCastModelList;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<PodCastModel> getPodCastModelList() {
        return podCastModelList;
    }

    public void setPodCastModelList(List<PodCastModel> podCastModelList) {
        this.podCastModelList = podCastModelList;
    }


}
