package info.appteve.lavradio.pojo;

public class FeatureImagesModel {

    private int id;
    private String title;
    private String description;
    private String imagePath;
    private String imageName;
    private String date;
    private String featuredUrl;
    private int featured;

    public FeatureImagesModel(int id, String title, String description, String imagePath, String imageName, String date, String featuredUrl, int featured) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imagePath = imagePath;
        this.imageName = imageName;
        this.date = date;
        this.featuredUrl = featuredUrl;
        this.featured = featured;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFeaturedUrl() {
        return featuredUrl;
    }

    public void setFeaturedUrl(String featuredUrl) {
        this.featuredUrl = featuredUrl;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }
}
