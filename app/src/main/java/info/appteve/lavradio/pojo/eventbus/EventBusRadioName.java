package info.appteve.lavradio.pojo.eventbus;

public class EventBusRadioName {

    private String name;

    public EventBusRadioName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
