
package info.appteve.lavradio.pojo.models.radio_models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RadioModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("radio_url")
    @Expose
    private String radioUrl;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("image_file")
    @Expose
    private String imageFile;
    @SerializedName("preroll")
    @Expose
    private List<Preroll> preroll = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRadioUrl() {
        return radioUrl;
    }

    public void setRadioUrl(String radioUrl) {
        this.radioUrl = radioUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageFile() {
        return imageFile;
    }

    public void setImageFile(String imageFile) {
        this.imageFile = imageFile;
    }

    public List<Preroll> getPreroll() {
        return preroll;
    }

    public void setPreroll(List<Preroll> preroll) {
        this.preroll = preroll;
    }

}
