package info.appteve.lavradio.pojo.eventbus;

public class EventBusPlayerStatus {

    private String status;

    public EventBusPlayerStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
