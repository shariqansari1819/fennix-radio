package info.appteve.lavradio.pojo.models.sidebar_models;

public class SideBarModels {

    private int image;
    private String title;

    public SideBarModels(int image, String title) {
        this.image = image;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        title = title;
    }
}
