package info.appteve.lavradio.pojo.models.offlien_models;

public class OfflineModel {

    private String name;
    private String date;
    private String url;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public OfflineModel() {
    }

    public OfflineModel(String name, String date, String url, String imageUrl) {
        this.name = name;
        this.date = date;
        this.url = url;
        this.imageUrl = imageUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
