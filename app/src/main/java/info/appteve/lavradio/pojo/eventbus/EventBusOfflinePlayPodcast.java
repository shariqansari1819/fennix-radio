package info.appteve.lavradio.pojo.eventbus;

import java.util.List;

import info.appteve.lavradio.pojo.models.offlien_models.OfflineModel;

public class EventBusOfflinePlayPodcast {

    private int position;
    private List<OfflineModel> offlineModelList;

    public EventBusOfflinePlayPodcast(int position, List<OfflineModel> offlineModelList) {
        this.position = position;
        this.offlineModelList = offlineModelList;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public List<OfflineModel> getOfflineModelList() {
        return offlineModelList;
    }

    public void setOfflineModelList(List<OfflineModel> offlineModelList) {
        this.offlineModelList = offlineModelList;
    }
}
