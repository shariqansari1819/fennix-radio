package info.appteve.lavradio;

import android.content.Context;

import android.os.AsyncTask;
import android.util.Log;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import info.appteve.lavradio.constants.Constants;

/**
 * Created by appteve on 11/01/2017.
 */

public class RadioInfoHelper implements Constants {


    static String getRadioForQuery(final RadioApiCallback apicallback, final Context context) {

        new AsyncTask<Void, Void, JSONObject>() {
            @Override
            protected JSONObject doInBackground(Void... unused) {

                String url = GENERAL_API_URL + URL_API_GET_LISTRADIO + KEY_STRING + API_KEY;

                JSONArray objectJson = Utilites.getJSONArrayFromUrl(url);

                try {

                    if (objectJson != null) {

                        JSONObject person = (JSONObject) objectJson
                                .get(0);


                        return person;


                    } else {

                        Log.v("INFO", "No items in Radio api Request");
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(final JSONObject objects) {

                if (objects != null) {

                    apicallback.finished(objects);

                } else {

                    apicallback.finished(null);
                }
            }
        }.execute();

        return null;

    }


    public interface RadioApiCallback {

        void finished(JSONObject object);

    }

    /// fetch news api

    static String getNewsForQuery(final NewsApiCallback apicallbacknews, final Context context) {

        new AsyncTask<Void, Void, JSONArray>() {
            @Override
            protected JSONArray doInBackground(Void... unused) {

                String url = GENERAL_API_URL + URL_API_GET_NEWS + API_KEY;

                JSONArray objectJson = Utilites.getJSONArrayFromUrl(url);


                if (objectJson != null) {

                    JSONArray ars = objectJson;

                    System.out.println(ars.toString());


                    return ars;


                } else {

                    Log.v("INFO", "No items in Radio api Request");
                }


                return null;
            }

            @Override
            protected void onPostExecute(final JSONArray objectsApi) {

                if (objectsApi != null) {

                    apicallbacknews.finishedNews(objectsApi);

                } else {


                    apicallbacknews.finishedNews(null);
                }
            }
        }.execute();

        return null;


    }


    public interface NewsApiCallback {

        void finishedNews(JSONArray objectz);

    }

    static String getFeaturedImages(final FeaturedImagesCallback featuredImagesCallback, final Context context) {

        new AsyncTask<Void, Void, JSONArray>() {
            @Override
            protected JSONArray doInBackground(Void... unused) {
                String url = GENERAL_API_URL + URL_FEATURED_IMAGES + API_KEY;
                JSONArray jsonArray = Utilites.getJSONArrayFromUrl(url);
                Log.e("Coming", "Starting task");
                if (jsonArray != null) {
                    JSONArray ars = jsonArray;

                    System.out.print(ars.toString());
                    return ars;
                } else {
                    Log.v("INFO", "No items in Radio api Request");
                }
                return null;
            }

            @Override
            protected void onPostExecute(final JSONArray objectsApi) {
                if (objectsApi != null) {
                    featuredImagesCallback.finishFeaturedImages(objectsApi);
                } else {
                    featuredImagesCallback.finishFeaturedImages(null);
                }
            }
        }.execute();

        return null;


    }

    interface FeaturedImagesCallback {
        void finishFeaturedImages(JSONArray jsonArray);
    }

    public static String getIpAddress(final IpAddressCallback ipAddressCallback, final Context context) {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... unused) {
                String url = GENERAL_API_URL + URL_IP_ADDRESS + API_KEY;
                String ipAddress = Utilites.getIpAddressFromUrl(url);
                if (ipAddress != null) {

                    return ipAddress;
                } else {
                    Log.v("INFO", "No items in Radio api Request");
                }
                return null;
            }

            @Override
            protected void onPostExecute(final String objectsApi) {
                if (objectsApi != null) {
                    ipAddressCallback.finishIpAddress(objectsApi);
                } else {
                    ipAddressCallback.finishIpAddress(null);
                }
            }
        }.execute();

        return null;


    }

    public interface IpAddressCallback {
        void finishIpAddress(String ipAddress);
    }

    public static void postListenStatus(int isActive, int listenTime, final PostListenCallback postListenCallback, Context context) {

        AndroidNetworking.post("http://bustosdashboard.fennixpro.com/endpoint/listener")
                .addHeaders("X-API-KEY", API_KEY)
                .addBodyParameter("is_active", String.valueOf(isActive))
                .addBodyParameter("listen_time", String.valueOf(listenTime))
                .setTag("listenTime")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        postListenCallback.finishPostListen(response);
                    }
                    @Override
                    public void onError(ANError error) {
                    }
                });
    }
    public interface PostListenCallback {
        void finishPostListen(JSONObject jsonObject);
    }

    /// fetch podcast api

    static String getPodcastForQuery(final PodcastApiCallback apicallbackpodcast, final Context context) {

        new AsyncTask<Void, Void, JSONArray>() {
            @Override
            protected JSONArray doInBackground(Void... unused) {

                String url = GENERAL_API_URL + URL_API_GET_PODCAST + API_KEY;
                Log.e("Coming", "Json Array");

                JSONArray objectJson = Utilites.getJSONArrayFromUrl(url);

                if (objectJson != null) {

                    JSONArray ars = objectJson;

                    System.out.println(ars.toString());


                    return ars;


                } else {

                    Log.v("INFO", "No items in Radio api Request");
                }


                return null;
            }

            @Override
            protected void onPostExecute(final JSONArray objectsApi) {

                if (objectsApi != null) {

                    apicallbackpodcast.finishedPodcast(objectsApi);

                } else {

                    apicallbackpodcast.finishedPodcast(null);
                }
            }
        }.execute();

        return null;


    }


    public interface PodcastApiCallback {

        void finishedPodcast(JSONArray objectz);

    }


}
