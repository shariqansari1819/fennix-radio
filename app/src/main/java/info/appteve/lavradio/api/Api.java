package info.appteve.lavradio.api;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import info.appteve.lavradio.endpoints.EndpointUrl;
import info.appteve.lavradio.pojo.models.radio_models.FeaturedModel;
import info.appteve.lavradio.pojo.models.radio_models.NewsModel;
import info.appteve.lavradio.pojo.models.radio_models.PodCastModel;
import info.appteve.lavradio.pojo.models.radio_models.RadioModel;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface Api {
    //        String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";
//    String XWWWORMURLENCODED = "application/x-www-form-urlencoded";
    String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";

    OkHttpClient.Builder httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Response response = chain.proceed(chain.request());
            // Do anything with response here
            response.header("Content-Type", APPLICATION_JSON_CHARSET_UTF_8);
            response.header("Accept", APPLICATION_JSON_CHARSET_UTF_8);
            return response;
        }
    }).addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).readTimeout(120, TimeUnit.SECONDS).connectTimeout(120, TimeUnit.SECONDS).retryOnConnectionFailure(true);

    OkHttpClient client = httpClient.build();

    Api WEB_SERVICE = new Retrofit.Builder()
            .baseUrl(EndpointUrl.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build().create(Api.class);

    @GET("/endpoint/featured/get")
    Call<List<FeaturedModel>> searchFeatured(@Header("X-API-KEY") String secret_key);

    @GET("/endpoint/news/get")
    Call<List<NewsModel>> searchNews(@Header("X-API-KEY") String secret_key);

    @GET("/endpoint/podcast/get")
    Call<List<PodCastModel>> searchPodCast(@Header("X-API-KEY") String secret_key);

    @GET("/endpoint/radio/getall")
    Call<List<RadioModel>> searchRadio(@Header("X-API-KEY") String secret_key);

//    @FormUrlEncoded
//    @POST("signup")
//    Call<RegisterLoginMainObject> signup(@Header("x-api-key") String secret_key, @Field("email") String email, @Field("pass") String password);
}