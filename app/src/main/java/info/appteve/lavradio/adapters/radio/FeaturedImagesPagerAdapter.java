package info.appteve.lavradio.adapters.radio;

import android.content.ContentResolver;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestOptions;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.makeramen.roundedimageview.RoundedImageView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import info.appteve.lavradio.R;
import info.appteve.lavradio.pojo.eventbus.EventBusFeaturedImageClick;
import info.appteve.lavradio.pojo.models.radio_models.FeaturedModel;
import pl.droidsonroids.gif.GifImageView;

public class FeaturedImagesPagerAdapter extends PagerAdapter {

    private List<FeaturedModel> featuredModels = new ArrayList<>();
    Context context;

    public FeaturedImagesPagerAdapter(List<FeaturedModel> images, Context context) {
        this.featuredModels = images;
        this.context = context;
    }

    @Override
    public int getCount() {
        return featuredModels.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.row_featured_images, container, false);
        RoundedImageView imageView = view.findViewById(R.id.imageViewFeatured);
        GifImageView gifImageView = view.findViewById(R.id.gifView);
        CardView cardView = view.findViewById(R.id.cardviewGif);

//        VideoView videoView = view.findViewById(R.id.videoView);
//        CardView cardView = view.findViewById(R.id.cardViewVideoFeaturedRow);
//        if (featuredModels.get(position).getImage().contains(".mp4")) {
//            videoView.setVisibility(View.VISIBLE);
//            cardView.setVisibility(View.VISIBLE);
//            imageView.setVisibility(View.GONE);
//            videoView.setVideoURI(Uri.parse(featuredModels.get(position).getImage()));
//            videoView.setOnPreparedListener(new OnPreparedListener() {
//                @Override
//                public void onPrepared() {
//                    videoView.start();
//                }
//            });
//        } else {
//            imageView.setVisibility(View.VISIBLE);
//            videoView.setVisibility(View.GONE);
//            cardView.setVisibility(View.GONE);
//            videoView.pause();
//        Uri uri = Uri.parse(featuredModels.get(position).getImage());
//        //Uri
//        ContentResolver contentResolver = null;//can be null for file:// Uris
//        GifDrawable gifFromUri = new GifDrawable(contentResolver, uri);
        if (featuredModels.get(position).getImage().contains(".gif")) {
            gifImageView.setVisibility(View.VISIBLE);
            cardView.setVisibility(View.VISIBLE);
            //Uri
//            ContentResolver contentResolver = ... //can be null for file:// Uris
//            GifDrawable gifFromUri = new GifDrawable( contentResolver, Uri.parse(featuredModels.get(position).getImage()) );

            Glide.with(context)
                    .load(featuredModels.get(position).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                    .into(gifImageView);
        } else {
            imageView.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(featuredModels.get(position).getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                    .into(imageView);
        }
        container.addView(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventBusFeaturedImageClick(position));
            }
        });
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
