package info.appteve.lavradio.adapters.radio;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.appteve.lavradio.R;
import info.appteve.lavradio.endpoints.EndpointUrl;
import info.appteve.lavradio.pojo.eventbus.EventBusPodCastClick;
import info.appteve.lavradio.pojo.models.radio_models.PodCastModel;

//Hassan PotCast Adapter

public class PodCastAdapter extends RecyclerView.Adapter<PodCastAdapter.ViewHolder> {

    private List<PodCastModel> potCastList;
    private Context context;

    public PodCastAdapter(List<PodCastModel> potCastList, Context context) {
        this.potCastList = potCastList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_news, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        PodCastModel model = potCastList.get(i);
        Glide.with(context)
                .load(EndpointUrl.IMAGE_BASE_URL + model.getImageFile())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                .apply(new RequestOptions().centerCrop())
                .into(viewHolder.appCompatImageView);
    }

    @Override
    public int getItemCount() {
        return potCastList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imageViewPodCast)
        ImageView appCompatImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            EventBus.getDefault().post(new EventBusPodCastClick(getAdapterPosition()));
        }
    }
}