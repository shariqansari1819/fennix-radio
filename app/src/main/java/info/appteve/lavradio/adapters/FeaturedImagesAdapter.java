package info.appteve.lavradio.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import info.appteve.lavradio.R;
import info.appteve.lavradio.pojo.FeatureImagesModel;
import info.appteve.lavradio.pojo.eventbus.EventBusFeaturedImageClick;

import static info.appteve.lavradio.constants.Constants.GENERAL_API_URL;
import static info.appteve.lavradio.constants.Constants.UPLOADS_FOLDER;

public class FeaturedImagesAdapter extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<FeatureImagesModel> imagesList = new ArrayList<>();

    public FeaturedImagesAdapter(Context context, ArrayList<FeatureImagesModel> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
        layoutInflater = LayoutInflater.from(context);
    }

    private int getBackgroundColor(int number) {
        switch (number) {
            case 0:
                return android.R.color.holo_red_light;
            case 1:
                return android.R.color.holo_orange_light;
            case 2:
                return android.R.color.holo_green_light;
            case 3:
                return android.R.color.holo_blue_light;
            case 4:
                return android.R.color.holo_purple;
            case 5:
                return android.R.color.black;
            default:
                return android.R.color.black;
        }
    }

    @Override
    public int getCount() {
        return imagesList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View view = layoutInflater.inflate(R.layout.item_pager_featured_image, container, false);
        ImageView imageView = view.findViewById(R.id.imageViewFeatureImageItem);
        FeatureImagesModel featureImagesModel = imagesList.get(position);
        if (featureImagesModel != null) {
            if (featureImagesModel.getImagePath() != null && !featureImagesModel.getImagePath().isEmpty()) {
                Picasso.with(context).load(GENERAL_API_URL + UPLOADS_FOLDER + imagesList.get(position).getImageName()).resize(container.getWidth(), container.getHeight()).centerCrop().into(imageView);
            }
        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new EventBusFeaturedImageClick(position));
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}