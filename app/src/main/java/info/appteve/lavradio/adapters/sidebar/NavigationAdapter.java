package info.appteve.lavradio.adapters.sidebar;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.appteve.lavradio.R;
import info.appteve.lavradio.pojo.eventbus.EventBusSideBarClick;
import info.appteve.lavradio.pojo.models.sidebar_models.SideBarModels;
import info.appteve.lavradio.utils.FontUtils;

public class NavigationAdapter extends RecyclerView.Adapter<NavigationAdapter.NavigationHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<SideBarModels> sideBarModelsList = new ArrayList<>();

    //    Font fields....
    private FontUtils fontUtils;

    public NavigationAdapter(Context context, List<SideBarModels> sideBarModelsList) {
        this.context = context;
        this.sideBarModelsList = sideBarModelsList;
        layoutInflater = LayoutInflater.from(context);
        fontUtils = FontUtils.getFontUtils(context);
    }

    @NonNull
    @Override
    public NavigationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.row_navigation, parent, false);
        return new NavigationHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NavigationHolder holder, int position) {
        SideBarModels sideBarModels = sideBarModelsList.get(position);
        if (sideBarModels != null) {
            Glide.with(context)
                    .load(sideBarModels.getImage())
                    .into(holder.imageViewThumbnail);
            holder.textViewTitle.setText(sideBarModels.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return sideBarModelsList.size();
    }

    class NavigationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imageViewNavigation)
        ImageView imageViewThumbnail;
        @BindView(R.id.textViewTitle)
        TextView textViewTitle;

        NavigationHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            fontUtils.setTextViewBoldFont(textViewTitle);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            EventBus.getDefault().post(new EventBusSideBarClick(getAdapterPosition()));
        }

    }
}
