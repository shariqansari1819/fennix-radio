package info.appteve.lavradio.adapters.radio;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.appteve.lavradio.R;
import info.appteve.lavradio.endpoints.EndpointUrl;
import info.appteve.lavradio.pojo.eventbus.EventBusRadioViewClick;
import info.appteve.lavradio.pojo.models.radio_models.RadioModel;
import info.appteve.lavradio.utils.FontUtils;

//Hassan  Favourites Adapter

public class RadioStreamAdapter extends RecyclerView.Adapter<RadioStreamAdapter.ViewHolder> {

    List<RadioModel> favouritesModelList;
    Context context;

    public RadioStreamAdapter(List<RadioModel> favouritesModelList, Context context) {
        this.favouritesModelList = favouritesModelList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_radio_stream, viewGroup, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        RadioModel model = favouritesModelList.get(i);
        viewHolder.textView.setText(model.getName());
        Glide.with(context)
                .load(EndpointUrl.IMAGE_BASE_URL+"/"+model.getImageFile())
                .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                .into(viewHolder.appCompatImageView);

    }

    @Override
    public int getItemCount() {
        return favouritesModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.imageViewRadioStream)
        AppCompatImageView appCompatImageView;
        @BindView(R.id.textViewRadioStream)
        TextView textView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            FontUtils.getFontUtils(context).setTextViewBoldFont(textView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            EventBus.getDefault().post(new EventBusRadioViewClick(getAdapterPosition()));
        }
    }
}
