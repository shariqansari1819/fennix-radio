package info.appteve.lavradio.adapters.offline;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.appteve.lavradio.R;
import info.appteve.lavradio.pojo.eventbus.EventBusDeleteOfflinePodcast;
import info.appteve.lavradio.pojo.eventbus.EventBusOfflineClick;
import info.appteve.lavradio.pojo.models.offlien_models.OfflineModel;
import info.appteve.lavradio.utils.FontUtils;

public class OfflineAdapter extends RecyclerView.Adapter<OfflineAdapter.ViewHolder> {

    private Context context;
    private List<OfflineModel> favouritesModelList;
    private FontUtils fontUtils;

    public OfflineAdapter(List<OfflineModel> favouritesModelList, Context context) {
        this.favouritesModelList = favouritesModelList;
        this.context = context;
        fontUtils = FontUtils.getFontUtils(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.row_offline, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        OfflineModel model = favouritesModelList.get(i);
        if (model != null) {
            viewHolder.textViewOfflineName.setText(model.getName());
            viewHolder.textViewOfflineDate.setText(model.getDate());
        }
    }

    @Override
    public int getItemCount() {
        return favouritesModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.textViewOfflineNameRowOffline)
        TextView textViewOfflineName;
        @BindView(R.id.textViewOfflineDateRowOffline)
        TextView textViewOfflineDate;
        @BindView(R.id.delete)
        ImageView delete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            fontUtils.setTextViewBoldFont(textViewOfflineName);
            fontUtils.setTextViewRegularFont(textViewOfflineDate);

            itemView.setOnClickListener(this);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.delete) {
                EventBus.getDefault().post(new EventBusDeleteOfflinePodcast(getAdapterPosition()));
            } else {
                EventBus.getDefault().post(new EventBusOfflineClick(getAdapterPosition()));
            }
        }
    }
}
