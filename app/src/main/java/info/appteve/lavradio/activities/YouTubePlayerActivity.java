package info.appteve.lavradio.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.devbrackets.android.exomedia.listener.OnCompletionListener;
import com.devbrackets.android.exomedia.listener.OnPreparedListener;
import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import info.appteve.lavradio.R;
import info.appteve.lavradio.common.Constants;
import info.appteve.lavradio.endpoints.EndpointKeys;
import info.appteve.lavradio.pojo.eventbus.EventBusMediaPlayer;
import info.appteve.lavradio.utils.ValidUtils;

public class YouTubePlayerActivity extends AppCompatActivity implements OnPreparedListener {

    //    Android fields....
    @BindView(R.id.youtubePlayerViewTrailer)
    YouTubePlayerView youTubePlayerView;
    @BindView(R.id.videoView)
    VideoView videoView;
    @BindView(R.id.textViewLearnMore)
    TextView textViewLearnMore;
    private YouTubePlayer youTubePlayer;

    //    Instance fields....
    private String videoUrl, playerType, learnMore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube_player);
        ButterKnife.bind(this);
        init();
    }

    public void init() {
        getLifecycle().addObserver(youTubePlayerView);

        if (getIntent() != null) {

            videoUrl = getIntent().getStringExtra(EndpointKeys.FEATURED_VIDEO_URL);
            playerType = getIntent().getStringExtra(EndpointKeys.PLAY_TYPE);
            learnMore = getIntent().getStringExtra(EndpointKeys.LEARN_MORE);

            if (!playerType.isEmpty()) {
                if (playerType.equals(Constants.RADIO_IMAGE)) {
                    textViewLearnMore.setVisibility(View.VISIBLE);
                } else {
                    textViewLearnMore.setVisibility(View.GONE);
                }
            }

            if (!videoUrl.equals("")) {
                if (videoUrl.contains("youtube")) {
                    youTubePlayerView.setVisibility(View.VISIBLE);
                    String newYoutubeKey = ValidUtils.getVideoId(videoUrl);
                    final String finalNewYoutubeKey = newYoutubeKey;
                    getLifecycle().addObserver(youTubePlayerView);
                    youTubePlayerView.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                        @Override
                        public void onReady(@NotNull YouTubePlayer youTubePlayer) {
                            super.onReady(youTubePlayer);
                            YouTubePlayerActivity.this.youTubePlayer = youTubePlayer;
                            youTubePlayer.loadVideo(finalNewYoutubeKey, 0);
                        }

                        @Override
                        public void onStateChange(@NotNull YouTubePlayer youTubePlayer, @NotNull PlayerConstants.PlayerState state) {
                            super.onStateChange(youTubePlayer, state);
                            switch (state) {
                                case ENDED:
                                    if (playerType.equals(Constants.RADIO_IMAGE)) {
                                        EventBus.getDefault().post(new EventBusMediaPlayer());
                                    }
                                    finish();
                                    break;
                            }
                        }
                    });

                } else {
                    String testURl = "http://androhub.com/demo/demo.mp4";
                    setupVideoView(videoUrl);
                }
            }
        }
    }

    @OnClick(R.id.textViewLearnMore)
    public void onLearnMoreClick(View view) {

        if (!learnMore.isEmpty()) {

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(learnMore));
            startActivity(Intent.createChooser(intent, "Choose browser"));// Choose browser is arbitrary :)

        } else {

        }

    }


    //setting Up video from the URL.
    private void setupVideoView(String URL) {

        videoView.setVisibility(View.VISIBLE);

        // Make sure to use the correct VideoView import
        videoView.setOnPreparedListener(this);
        videoView.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion() {
                if (playerType.equals(Constants.RADIO_IMAGE)) {
                    EventBus.getDefault().post(new EventBusMediaPlayer());
                }
                finish();
            }
        });
        //For now we just picked an arbitrary item to play
        videoView.setVideoURI(Uri.parse(URL));
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (youTubePlayer != null) {
            youTubePlayer.pause();
        }
        if (videoView != null) {
            videoView.pause();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (youTubePlayer != null) {
            youTubePlayer.play();
        }
        if (videoView != null) {
            videoView.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        youTubePlayerView.release();
    }

    @Override
    public void onPrepared() {
        videoView.start();
    }

}