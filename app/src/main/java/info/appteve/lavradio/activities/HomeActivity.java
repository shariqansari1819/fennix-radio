package info.appteve.lavradio.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;
import info.appteve.lavradio.R;
import info.appteve.lavradio.endpoints.EndpointKeys;
import info.appteve.lavradio.fragments.InAppBrowserFragment;
import info.appteve.lavradio.fragments.NewsBrowserFragment;
import info.appteve.lavradio.fragments.OfflineFragment;
import info.appteve.lavradio.fragments.OfflinePlayerFragment;
import info.appteve.lavradio.fragments.PodcastPlayerFragment;
import info.appteve.lavradio.fragments.RadioFragment;
import info.appteve.lavradio.fragments.base.BaseFragment;
import info.appteve.lavradio.pojo.eventbus.EventBusSideBarClick;
import info.appteve.lavradio.pojo.eventbus.EventBusStoragePermissionResult;

import static info.appteve.lavradio.utils.backstack.FragmentUtils.addAdditionalTabFragment;
import static info.appteve.lavradio.utils.backstack.FragmentUtils.addInitialTabFragment;
import static info.appteve.lavradio.utils.backstack.FragmentUtils.addShowHideFragment;
import static info.appteve.lavradio.utils.backstack.FragmentUtils.removeFragment;
import static info.appteve.lavradio.utils.backstack.FragmentUtils.showHideTabFragment;
import static info.appteve.lavradio.utils.backstack.StackListManager.updateStackIndex;
import static info.appteve.lavradio.utils.backstack.StackListManager.updateStackToIndexFirst;
import static info.appteve.lavradio.utils.backstack.StackListManager.updateTabStackIndex;

public class HomeActivity extends AppCompatActivity implements BaseFragment.FragmentInteractionCallback {


    //    Android fields....
    @BindView(R.id.drawerLayoutMain)
    DrawerLayout drawerLayoutMain;
    @BindView(R.id.appBarHome)
    Toolbar toolbarMain;
    @BindView(R.id.imageViewLogoAppBarHome)
    ImageView imageViewLogo;
    ActionBarDrawerToggle actionBarDrawerToggle;

    //    Fragment fields...
    private RadioFragment radioFragment;
    private OfflineFragment offlineFragment;
    private InAppBrowserFragment inAppBrowserFragment;
    private NewsBrowserFragment newsBrowserFragment;
    private Fragment currentFragment;
    private PodcastPlayerFragment podcastPlayerFragment;
    private OfflinePlayerFragment offlinePlayerFragment;

    //    Instance fields....
    private int index;

    //    Stack fields....
    private Map<String, Stack<Fragment>> stacks;
    private String currentTab;
    private List<String> stackList;
    private List<String> menuStacks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            View decor = getWindow().getDecorView();
//            if (true) {
//                decor.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
//            } else {
//                // We want to change tint color to white again.
//                // You can also record the flags in advance so that you can turn UI back completely if
//                // you have set other flags before, such as translucent or full screen.
//                decor.setSystemUiVisibility(0);
//            }
//        }

        setSupportActionBar(toolbarMain);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            Glide.with(this).load(R.drawable.logo).into(imageViewLogo);
        }

//        Setting drawer layout....
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayoutMain, toolbarMain, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayoutMain.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

//        Fragment manager fields initialization....
        initializeFragments();

//        Creating stacks....
        createStacks();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBusSideBarClick(EventBusSideBarClick eventBusSideBarClick) {
        drawerLayoutMain.closeDrawer(GravityCompat.START);
        switch (eventBusSideBarClick.getPosition()) {
            case 0:
                selectedTab(EndpointKeys.RADIO);
                break;
            case 1:
                selectedTab(EndpointKeys.OFFLINE);
                break;
            case 2:
                selectedTab(EndpointKeys.IN_APP_BROWSER);
                break;
        }
    }

    private void initializeFragments() {
        radioFragment = new RadioFragment();
        offlineFragment = new OfflineFragment();
        inAppBrowserFragment = new InAppBrowserFragment();
        newsBrowserFragment = new NewsBrowserFragment();
        podcastPlayerFragment = new PodcastPlayerFragment();
        offlinePlayerFragment = new OfflinePlayerFragment();
    }

    private void createStacks() {
        stacks = new LinkedHashMap<>();
        stacks.put(EndpointKeys.RADIO, new Stack<Fragment>());
        stacks.put(EndpointKeys.OFFLINE, new Stack<Fragment>());
        stacks.put(EndpointKeys.IN_APP_BROWSER, new Stack<Fragment>());
        stacks.put(EndpointKeys.NEWS_BROWSER, new Stack<Fragment>());
        stacks.put(EndpointKeys.PODCAST_PLAYER, new Stack<Fragment>());
        stacks.put(EndpointKeys.OFFLINE_PODCAST_PLAYER, new Stack<Fragment>());

        menuStacks = new ArrayList<>();
        menuStacks.add(EndpointKeys.RADIO);

        stackList = new ArrayList<>();
        stackList.add(EndpointKeys.RADIO);
        stackList.add(EndpointKeys.OFFLINE);
        stackList.add(EndpointKeys.IN_APP_BROWSER);
        stackList.add(EndpointKeys.NEWS_BROWSER);
        stackList.add(EndpointKeys.PODCAST_PLAYER);
        stackList.add(EndpointKeys.OFFLINE_PODCAST_PLAYER);

//        imageViews[0].setSelected(true);
//        setAppBarTitle(getResources().getString(R.string.trending));
        selectedTab(EndpointKeys.RADIO);
    }

    public void selectedTab(String tabId) {

        currentTab = tabId;
        BaseFragment.setCurrentTab(currentTab);

        if (stacks.get(tabId).size() == 0) {
            /*
             * First time this tab is selected. So add first fragment of that tab.
             * We are adding a new fragment which is not present in stack. So add to stack is true.
             */
            switch (tabId) {
                case EndpointKeys.RADIO:
                    addInitialTabFragment(getSupportFragmentManager(), stacks, EndpointKeys.RADIO, radioFragment, R.id.frameLayoutFragmentContainer, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(radioFragment);
                    break;
                case EndpointKeys.OFFLINE:
                    addAdditionalTabFragment(getSupportFragmentManager(), stacks, EndpointKeys.OFFLINE, offlineFragment, currentFragment, R.id.frameLayoutFragmentContainer, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(offlineFragment);
                    break;
                case EndpointKeys.IN_APP_BROWSER:
                    addAdditionalTabFragment(getSupportFragmentManager(), stacks, EndpointKeys.IN_APP_BROWSER, inAppBrowserFragment, currentFragment, R.id.frameLayoutFragmentContainer, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(inAppBrowserFragment);
                    break;
                case EndpointKeys.NEWS_BROWSER:
                    addAdditionalTabFragment(getSupportFragmentManager(), stacks, EndpointKeys.NEWS_BROWSER, newsBrowserFragment, currentFragment, R.id.frameLayoutFragmentContainer, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(newsBrowserFragment);
                    break;
                case EndpointKeys.PODCAST_PLAYER:
                    addAdditionalTabFragment(getSupportFragmentManager(), stacks, EndpointKeys.PODCAST_PLAYER, podcastPlayerFragment, currentFragment, R.id.frameLayoutFragmentContainer, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(podcastPlayerFragment);
                    break;
                case EndpointKeys.OFFLINE_PODCAST_PLAYER:
                    addAdditionalTabFragment(getSupportFragmentManager(), stacks, EndpointKeys.OFFLINE_PODCAST_PLAYER, offlinePlayerFragment, currentFragment, R.id.frameLayoutFragmentContainer, true);
                    resolveStackLists(tabId);
                    assignCurrentFragment(offlinePlayerFragment);
                    break;
            }
        } else {
            /*
             * We are switching tabs, and target tab already has at least one fragment.
             * Show the target fragment
             */
            showHideTabFragment(getSupportFragmentManager(), stacks.get(tabId).lastElement(), currentFragment);
            resolveStackLists(tabId);
            assignCurrentFragment(stacks.get(tabId).lastElement());
        }
    }

    private void popFragment() {
        /*
         * Select the second last fragment in current tab's stack,
         * which will be shown after the fragment transaction given below
         */
        Fragment fragment = stacks.get(currentTab).elementAt(stacks.get(currentTab).size() - 4);

        /*pop current fragment from stack */
        stacks.get(currentTab).pop();

        removeFragment(getSupportFragmentManager(), fragment, currentFragment);

        assignCurrentFragment(fragment);
    }

    private void resolveBackPressed() {
        int stackValue = 0;
        if (currentTab.equals(EndpointKeys.PODCAST_PLAYER)) {
            if (podcastPlayerFragment.mediaPlayer != null) {
                podcastPlayerFragment.mediaPlayer.reset();
                podcastPlayerFragment.mediaPlayer.release();
                podcastPlayerFragment.mediaPlayer = null;
            }
        }
        if (currentTab.equals(EndpointKeys.OFFLINE_PODCAST_PLAYER)) {
            if (offlinePlayerFragment.mediaPlayer != null) {
                offlinePlayerFragment.mediaPlayer.reset();
                offlinePlayerFragment.mediaPlayer.release();
                offlinePlayerFragment.mediaPlayer = null;
            }
        }
        if (stacks.get(currentTab).size() == 1) {
            Stack<Fragment> value = stacks.get(stackList.get(1));
            if (value.size() > 1) {
                stackValue = value.size();
                popAndNavigateToPreviousMenu();
            }
            if (stackValue <= 1) {
                if (menuStacks.size() > 1) {
                    navigateToPreviousMenu();
                } else {
                    finish();
                }
            }
        } else {
            popFragment();
        }
    }

    /*Pops the last fragment inside particular tab and goes to the second tab in the stack*/
    private void popAndNavigateToPreviousMenu() {
        String tempCurrent = stackList.get(0);
        currentTab = stackList.get(1);
        BaseFragment.setCurrentTab(currentTab);
        resolveTabPositions(currentTab);
        showHideTabFragment(getSupportFragmentManager(), stacks.get(currentTab).lastElement(), currentFragment);
        assignCurrentFragment(stacks.get(currentTab).lastElement());
        updateStackToIndexFirst(stackList, tempCurrent);
        menuStacks.remove(0);
    }

    private void navigateToPreviousMenu() {
        menuStacks.remove(0);
        currentTab = menuStacks.get(0);
        BaseFragment.setCurrentTab(currentTab);
        resolveTabPositions(currentTab);
        showHideTabFragment(getSupportFragmentManager(), stacks.get(currentTab).lastElement(), currentFragment);
        assignCurrentFragment(stacks.get(currentTab).lastElement());
    }

    public void showFragment(Fragment fragmentToAdd, String tag, boolean addTostack) {
//        String tab = bundle.getString(DATA_KEY_1);
//        boolean shouldAdd = bundle.getBoolean(DATA_KEY_2);
        addShowHideFragment(getSupportFragmentManager(), stacks, tag, fragmentToAdd, getCurrentFragmentFromShownStack(), R.id.frameLayoutFragmentContainer, addTostack);
        assignCurrentFragment(fragmentToAdd);
    }

    private int resolveTabPositions(String currentTab) {
        switch (currentTab) {
            case EndpointKeys.RADIO:
                index = 0;
//                setAppBarTitle(getResources().getString(R.string.trending));
                break;
            case EndpointKeys.OFFLINE:
                index = 1;
//                setAppBarTitle(getResources().getString(R.string.upcoming_movies));
                break;
            case EndpointKeys.IN_APP_BROWSER:
                index = 2;
//                setAppBarTitle(getResources().getString(R.string.top_rated_movies));
                break;
            case EndpointKeys.NEWS_BROWSER:
                index = 3;
                break;
            case EndpointKeys.PODCAST_PLAYER:
                index = 4;
                break;
            case EndpointKeys.OFFLINE_PODCAST_PLAYER:
                index = 5;
                break;
        }
//        if (index == 2) {
//            textViewAppBarTitle.setVisibility(View.GONE);
//            editTextSearchAppBar.setVisibility(View.VISIBLE);
//        } else {
//            textViewAppBarTitle.setVisibility(View.VISIBLE);
//            editTextSearchAppBar.setVisibility(View.GONE);
//        }
//        imageViews[currentFragmentIndex].setSelected(false);
//        currentFragmentIndex = index;
        return index;
    }

    private void resolveStackLists(String tabId) {
        updateStackIndex(stackList, tabId);
        updateTabStackIndex(menuStacks, tabId);
    }

    private Fragment getCurrentFragmentFromShownStack() {
        return stacks.get(currentTab).elementAt(stacks.get(currentTab).size() - 1);
    }

    private void assignCurrentFragment(Fragment current) {
        currentFragment = current;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayoutMain.isDrawerOpen(GravityCompat.START)) {
            drawerLayoutMain.closeDrawer(GravityCompat.START);
        } else {
            resolveBackPressed();
        }
    }

    @Override
    public void onFragmentInteractionCallback(Bundle bundle) {

    }

    public boolean checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    public void requestStoragePermission(int resultCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, resultCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int count = 0;
        for (int result : grantResults) {
            if (result == PackageManager.PERMISSION_GRANTED) {
                count++;
            }
        }
        if (count == grantResults.length) {
            EventBus.getDefault().post(new EventBusStoragePermissionResult(requestCode));
        }
    }
}
