package info.appteve.lavradio.endpoints;

public interface EndpointUrl {
    String API_BASE_URL = "http://bustosdashboard.fennixpro.com";
    String IMAGE_BASE_URL = "http://bustosdashboard.fennixpro.com/uploads/";
    String X_API_KEY = "testkey";
}
