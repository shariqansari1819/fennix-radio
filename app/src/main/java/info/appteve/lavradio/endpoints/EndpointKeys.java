package info.appteve.lavradio.endpoints;

public interface EndpointKeys {

    String RADIO = "radio";
    String OFFLINE = "offline";
    String IN_APP_BROWSER = "in_app_browser";
    String NEWS_BROWSER = "news_browser";
    String PODCAST_PLAYER = "podcast_player";
    String OFFLINE_PODCAST_PLAYER = "offline_podcast_player";

    String FEATURED_VIDEO_URL = "featured_video_url";
    String DOWNLOAD_MEDIA = "";
    String DOWNLOAD_DIRECTORY = "Downloads";
    String PLAY_TYPE = "play_type";
    String LEARN_MORE = "learn_more";

}
